insert into permissao (id, data_cadastro, nome)
values (nextval('permissao_seq'), CURRENT_DATE, 'admin');

insert into usuario_permissoes (id_usuario, id_permissao)
values ('admin', 1);