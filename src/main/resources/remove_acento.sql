CREATE OR REPLACE FUNCTION remove_acento(text)
  RETURNS text AS
$BODY$ 
  select translate($1,'��������������������������������������������', 
  					  'aaaaaeeeeiiiooooouuuuAAAAAEEEEIIIOOOOOUUUUcC'); 
$BODY$
  LANGUAGE sql IMMUTABLE STRICT
  COST 100;
ALTER FUNCTION remove_acento(text)
  OWNER TO postgres;
