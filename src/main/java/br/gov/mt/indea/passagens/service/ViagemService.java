package br.gov.mt.indea.passagens.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.passagens.entity.dto.AbstractDTO;
import br.gov.mt.indea.passagens.entity.dto.ViagemDTO;
import br.gov.mt.indea.passagens.entity.passagens.Usuario;
import br.gov.mt.indea.passagens.entity.passagens.Viagem;
import br.gov.mt.indea.passagens.entity.passagens.ViagemAnexo;
import br.gov.mt.indea.passagens.enums.Dominio.Status;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class ViagemService extends PaginableService<Viagem, Long> {
	
	private static final Logger log = LoggerFactory.getLogger(ViagemService.class);
	
	@Inject
	ServidorService servidorService;
	
	protected ViagemService() {
		super(Viagem.class);
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Viagem findById(Long id){
		Viagem viagem;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select viagem ")
		   .append("  from Viagem viagem ")
		   .append("  left join fetch viagem.servidor ")
		   .append("  left join fetch viagem.viagemIda ")
		   .append("  left join fetch viagem.coordenadoriaDemandante ")
		   .append(" where viagem.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		viagem = (Viagem) query.uniqueResult();
		
		if (viagem != null){
			if (viagem.getServidor() != null && viagem.getServidor().getIdServidor() != null)
				viagem.getServidor().setServidorIndeaWeb(servidorService.findByIdFetchAll(Long.valueOf(viagem.getServidor().getIdServidor())));
			
			if (viagem.getUsuarioAutorizador() != null && viagem.getUsuarioAutorizador().getServidor() != null && viagem.getUsuarioAutorizador().getServidor().getIdServidor() != null)
				viagem.getUsuarioAutorizador().getServidor().setServidorIndeaWeb(servidorService.findByIdFetchAll(Long.valueOf(viagem.getUsuarioAutorizador().getServidor().getIdServidor())));
		}
		
		return viagem;
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Viagem findByCodigoFetchALl(String codigo) {
		Viagem viagem;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select viagem ")
		   .append("  from Viagem viagem ")
		   .append("  left join fetch viagem.servidor ")
		   .append("  left join fetch viagem.coordenadoriaDemandante ")
		   .append(" where viagem.assinaturaEletronica = :assinaturaEletronica ");
		
		Query query = getSession().createQuery(sql.toString()).setString("assinaturaEletronica", codigo);
		viagem = (Viagem) query.uniqueResult();
		
		if (viagem != null){
			if (viagem.getServidor() != null && viagem.getServidor().getIdServidor() != null)
				viagem.getServidor().setServidorIndeaWeb(servidorService.findByIdFetchAll(Long.valueOf(viagem.getServidor().getIdServidor())));
			
			if (viagem.getUsuarioAutorizador() != null && viagem.getUsuarioAutorizador().getServidor() != null && viagem.getUsuarioAutorizador().getServidor().getIdServidor() != null)
				viagem.getUsuarioAutorizador().getServidor().setServidorIndeaWeb(servidorService.findByIdFetchAll(Long.valueOf(viagem.getUsuarioAutorizador().getServidor().getIdServidor())));
		}
		
		return viagem;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ViagemAnexo getViagemAnexo(Viagem viagem) {
		ViagemAnexo viagemAnexo = null;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select viagemAnexo ")
		   .append("  from ViagemAnexo viagemAnexo ")
		   .append("  left join viagemAnexo.viagem viagem")
		   .append(" where viagem.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", viagem.getId());
		viagemAnexo = (ViagemAnexo) query.uniqueResult();
		
		return viagemAnexo;
		
	}
	
//	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
//	public TrechoAnexo getTrechoAnexo(Trecho trecho) {
//		TrechoAnexo trechoAnexo = null;
//		
//		StringBuilder sql = new StringBuilder();
//		sql.append("select trechoAnexo ")
//		   .append("  from TrechoAnexo trechoAnexo ")
//		   .append("  left join trechoAnexo.trecho trecho")
//		   .append(" where trecho.id = :id ");
//		
//		Query query = getSession().createQuery(sql.toString()).setLong("id", trecho.getId());
//		trechoAnexo = (TrechoAnexo) query.uniqueResult();
//		
//		return trechoAnexo;
//		
//	}
	
	@SuppressWarnings("unchecked")
	public List<Viagem> findAllParaImpressao(Usuario usuario) {
		List<Viagem> listaViagem;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select viagem ")
		   .append("  from Viagem viagem ")
		   .append("  left join fetch viagem.servidor ")
		   .append(" where viagem.servidor = :servidor ")
		   .append("   and viagem.status = :status ");
		
		Query query = getSession().createQuery(sql.toString()).setParameter("servidor", usuario.getServidor()).setParameter("status", Status.EMITIDA);
		listaViagem = query.list();
		
		if (listaViagem != null){
		}
		
		return listaViagem;
	}
	
	@Override
	public Long countAll(AbstractDTO dto) {
		StringBuilder sql = new StringBuilder();
		sql.append("select count(entity) ")
		   .append("  from " + this.getType().getSimpleName() + " as entity")
		   .append(" where 1 = 1");
		
		if (dto != null && !dto.isNull()){
			ViagemDTO viagemDTO = (ViagemDTO) dto;
			
			if (!StringUtils.isEmpty(viagemDTO.getOs()))
				sql.append(" AND entity.os = :os");
			
			if(viagemDTO.getStatus() != null)
				sql.append(" AND entity.status like :status ");
			
			if (viagemDTO.getCoordenadoriaDemandante() != null)
				sql.append(" AND entity.coordenadoriaDemandante = :coordenadoriaDemandante ");
			else if (viagemDTO.getListaCoordenadoriaDemandante() != null)
				sql.append(" AND entity.coordenadoriaDemandante IN :coordenadoriaDemandante ");
		}
		
		Query query = getSession().createQuery(sql.toString());
		
		if (dto != null && !dto.isNull()){
			ViagemDTO viagemDTO = (ViagemDTO) dto;
			
			if (!StringUtils.isEmpty(viagemDTO.getOs()))
				query.setString("os", viagemDTO.getOs());
			
			if(viagemDTO.getStatus() != null)
				query.setParameter("status", viagemDTO.getStatus());
			
			if (viagemDTO.getCoordenadoriaDemandante() != null)
				query.setParameter("coordenadoriaDemandante", viagemDTO.getCoordenadoriaDemandante());
			else if (viagemDTO.getListaCoordenadoriaDemandante() != null)
				query.setParameterList("coordenadoriaDemandante", viagemDTO.getListaCoordenadoriaDemandante());
		}
		
		return (Long) query.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<Viagem> findAllComPaginacao(AbstractDTO dto, int first, int pageSize, String sortField, String sortOrder) {
		StringBuilder sql = new StringBuilder();
		sql.append("select entity ")
		   .append("  from " + this.getType().getSimpleName() + " as entity")
		   .append(" where 1 = 1");
		
		if (dto != null && !dto.isNull()){
			ViagemDTO viagemDTO = (ViagemDTO) dto;
			
			if (!StringUtils.isEmpty(viagemDTO.getOs()))
				sql.append(" AND entity.os = :os");
		
			if(viagemDTO.getStatus() != null)
				sql.append(" AND entity.status like :status ");
			
			if (viagemDTO.getCoordenadoriaDemandante() != null)
				sql.append(" AND entity.coordenadoriaDemandante = :coordenadoriaDemandante ");
			else if (viagemDTO.getListaCoordenadoriaDemandante() != null)
				sql.append(" AND entity.coordenadoriaDemandante IN :coordenadoriaDemandante ");
		}
		
		if (StringUtils.isNotBlank(sortField))
			sql.append(" order by entity." + sortField + " " + sortOrder);

		Query query = getSession().createQuery(sql.toString());
		
		if (dto != null && !dto.isNull()){
			ViagemDTO viagemDTO = (ViagemDTO) dto;
			
			if (!StringUtils.isEmpty(viagemDTO.getOs()))
				query.setString("os", viagemDTO.getOs());
			
			if(viagemDTO.getStatus() != null)
				query.setParameter("status", viagemDTO.getStatus());
			
			if (viagemDTO.getCoordenadoriaDemandante() != null)
				query.setParameter("coordenadoriaDemandante", viagemDTO.getCoordenadoriaDemandante());
			else if (viagemDTO.getListaCoordenadoriaDemandante() != null)
				query.setParameterList("coordenadoriaDemandante", viagemDTO.getListaCoordenadoriaDemandante());
		}

		query.setFirstResult(first);
		query.setMaxResults(pageSize);

		List<Viagem> lista = query.list();
		
		return lista;
    }
	
	@Override
	public void saveOrUpdate(Viagem viagem) {
		viagem = (Viagem) this.getSession().merge(viagem);
		
		super.saveOrUpdate(viagem);
		
		log.info("Salvando Viagem {}", viagem.getId());
	}
	
	@Override
	public void delete(Viagem viagem) {
		super.delete(viagem);
		
		log.info("Removendo Viagem {}", viagem.getId());
	}
	
	@Override
	public void validar(Viagem Viagem) {

	}

	@Override
	public void validarPersist(Viagem Viagem) {

	}

	@Override
	public void validarMerge(Viagem Viagem) {

	}

	@Override
	public void validarDelete(Viagem Viagem) {

	}

}
