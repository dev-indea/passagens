package br.gov.mt.indea.passagens.converter.jsf;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import br.gov.mt.indea.passagens.enums.Dominio.AtivoInativo;

@FacesConverter(forClass=AtivoInativo.class)
public class AtivoInativoConverter implements Converter {

	@Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value.equals("true"))
        	return AtivoInativo.ATIVO;
        else
        	return AtivoInativo.INATIVO;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
    	AtivoInativo ativoInativo = (AtivoInativo) value;
    	if (ativoInativo.equals(AtivoInativo.ATIVO))
    		return "true";
    	else
    		return "false";
    }

}