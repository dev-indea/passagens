package br.gov.mt.indea.passagens.entity.passagens;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.passagens.entity.dbindea.Unidade;
import br.gov.mt.indea.passagens.enums.Dominio.AtivoInativo;

@Audited
@Entity
@Table(name = "coordenadoria_demandante")
public class CoordenadoriaDemandante extends BaseEntity<Long> implements Serializable{

	private static final long serialVersionUID = -8530781402902841303L;

	@Id
	@SequenceGenerator(name="coordenadoria_demandante_seq", sequenceName="coordenadoria_demandante_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="coordenadoria_demandante_seq")
	private Long id;
	
	@Column(name="data_cadastro")
	private LocalDateTime dataCadastro;
	
	@Column(name="id_unidade", length = 255)
	private String idUnidade;
	
	@Column(name="nome_unidade", length = 255)
	private String nomeUnidade;
	
	@Transient
	private Unidade unidade;
	
	@ManyToOne(cascade = {CascadeType.DETACH, CascadeType.REFRESH}, fetch = FetchType.EAGER)
	@JoinColumn(name = "id_usuario_responsavel")
	private Usuario usuarioResponsavel;
	
	@Enumerated(EnumType.STRING)
	@Column(name="status")
	private AtivoInativo status = AtivoInativo.ATIVO;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDateTime getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(LocalDateTime dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public String getIdUnidade() {
		return idUnidade;
	}

	public void setIdUnidade(String idUnidade) {
		this.idUnidade = idUnidade;
	}

	public String getNomeUnidade() {
		return nomeUnidade;
	}

	public void setNomeUnidade(String nomeUnidade) {
		this.nomeUnidade = nomeUnidade;
	}

	public Unidade getUnidade() {
		return unidade;
	}

	public void setUnidade(Unidade unidade) {
		this.idUnidade = unidade.getId() + "";
		this.nomeUnidade = unidade.getNome();
		this.unidade = unidade;
	}

	public Usuario getUsuarioResponsavel() {
		return usuarioResponsavel;
	}

	public void setUsuarioResponsavel(Usuario usuarioResponsavel) {
		this.usuarioResponsavel = usuarioResponsavel;
	}

	public AtivoInativo getStatus() {
		return status;
	}

	public void setStatus(AtivoInativo status) {
		this.status = status;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CoordenadoriaDemandante other = (CoordenadoriaDemandante) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
