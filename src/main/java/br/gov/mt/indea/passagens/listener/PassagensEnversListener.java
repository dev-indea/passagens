package br.gov.mt.indea.passagens.listener;

import org.hibernate.envers.RevisionListener;

import br.gov.mt.indea.passagens.entity.envers.PassagensRevisionEntity;
import br.gov.mt.indea.passagens.security.UserSecurity;
import br.gov.mt.indea.passagens.util.CDIServiceLocator;

public class PassagensEnversListener implements RevisionListener{
	
	@Override
	public void newRevision(Object revisionEntity) {
		PassagensRevisionEntity entity = (PassagensRevisionEntity) revisionEntity;
		
		UserSecurity userSecurity = CDIServiceLocator.getBean(UserSecurity.class);
		
		if (userSecurity != null){
			entity.setUsername(userSecurity.getUsuario().getId());
			entity.setUsuario(userSecurity.getUsuario().getNome());
			entity.setIp(userSecurity.getIp());
		} else{
			entity.setUsername("anonymous");
			entity.setUsuario("anonymous");
		}
		
	}

}
