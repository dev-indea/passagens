package br.gov.mt.indea.passagens.managedbeans;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.passagens.entity.dbindea.Servidor;
import br.gov.mt.indea.passagens.entity.dto.ViagemDTO;
import br.gov.mt.indea.passagens.entity.passagens.CoordenadoriaDemandante;
import br.gov.mt.indea.passagens.entity.passagens.Municipio;
import br.gov.mt.indea.passagens.entity.passagens.UF;
import br.gov.mt.indea.passagens.entity.passagens.Viagem;
import br.gov.mt.indea.passagens.enums.Dominio.Status;
import br.gov.mt.indea.passagens.enums.Dominio.TipoViagem;
import br.gov.mt.indea.passagens.exception.ApplicationErrorException;
import br.gov.mt.indea.passagens.security.UserSecurity;
import br.gov.mt.indea.passagens.service.CoordenadoriaDemandanteService;
import br.gov.mt.indea.passagens.service.LazyObjectDataModel;
import br.gov.mt.indea.passagens.service.MunicipioService;
import br.gov.mt.indea.passagens.service.ServidorService;
import br.gov.mt.indea.passagens.service.ViagemService;
import br.gov.mt.indea.passagens.util.FacesMessageUtil;

@Named
@ViewScoped
@URLBeanName("viagemManagedBean")
@URLMappings(mappings = {
		@URLMapping(id = "pesquisarViagem", pattern = "/viagem/pesquisar", viewId = "/pages/viagem/lista.jsf"),
		@URLMapping(id = "incluirViagem", pattern = "/viagem/incluir", viewId = "/pages/viagem/novo.jsf"),
		@URLMapping(id = "alterarViagem", pattern = "/viagem/alterar/#{viagemManagedBean.id}", viewId = "/pages/viagem/novo.jsf"),
		@URLMapping(id = "solicitarVoltaViagem", pattern = "/viagem/volta/#{viagemManagedBean.id}", viewId = "/pages/viagem/novo.jsf"),
		@URLMapping(id = "impressaoViagem", pattern = "/viagem/#{viagemManagedBean.id}/impressao/#{viagemManagedBean.tipo}", viewId = "/pages/impressao.jsf")})
public class ViagemManagedBean implements Serializable {

	private static final long serialVersionUID = 5401332308697941357L;
	
	private Long id;
	
	private String tipo;
	
	@Inject
	private Viagem viagem;
	
	@Inject
	private ViagemDTO viagemDTO;
	
	boolean editando = false;
	
	private LazyObjectDataModel<Viagem> listaViagem;
	
	private List<Servidor> listaServidores;
	
	@Inject
	private ViagemService viagemService;
	
	@Inject
	private ServidorService servidorService;
	
	@Inject
	private MunicipioService municipioService;
	
	@Inject
	private CoordenadoriaDemandanteService coordenadoriaDemandanteService;
	
	@Inject
	private UserSecurity userSecurity;

	private UF ufOrigem = UF.MATO_GROSSO;
	
	private UF ufDestino = UF.MATO_GROSSO;
	
	@PostConstruct
	private void init(){
	}
	
	public void limpar(){
		this.viagem = new Viagem();
		
		this.editando = false;
	}
	
	public void buscarViagens(){
		if (this.viagemDTO.getCoordenadoriaDemandante() == null)
			this.viagemDTO.setListaCoordenadoriaDemandante(this.getListaCoordenadoriaDemandante());
		else
			this.viagemDTO.setListaCoordenadoriaDemandante(null);
		
		this.listaViagem = new LazyObjectDataModel<Viagem>(this.viagemService, this.viagemDTO);
		
	}
	
	@URLAction(mappingId = "incluirViagem", onPostback = false)
	public void novo(){
		this.limpar();
		
		this.viagem = new Viagem(this.userSecurity.getUsuario().getServidor());
	}
	
	@URLAction(mappingId = "pesquisarViagem", onPostback = false)
	public void pesquisar(){
		limpar();
		
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		String osViagem = (String) request.getSession().getAttribute("osViagem");
		
		if (!StringUtils.isEmpty(osViagem)){
			this.viagemDTO = new ViagemDTO();
			this.viagemDTO.setOs(osViagem);
			this.buscarViagens();
			
			request.getSession().setAttribute("osViagem", null);
		}
	}
	
	public List<Servidor> buscarServidor(String query){
		
		listaServidores = servidorService.findByNome(query);
		return listaServidores;
	}

	@URLAction(mappingId = "alterarViagem", onPostback = false)
	public void editar(){
		this.editando = true;
		this.viagem = viagemService.findById(this.getId());
		this.setUfOrigem(this.viagem.getMunicipioOrigem().getUf());
		this.setUfDestino(this.viagem.getMunicipioDestino().getUf());
	}
	
	public String adicionar(){
		
		if (this.viagem.getId() == null){
			this.viagem.setDataCadastro(LocalDateTime.now());
			this.viagem.setStatus(Status.NOVA);
			this.viagemService.saveOrUpdate(viagem);
			FacesMessageUtil.addInfoContextFacesMessage("Viagem OS n� " + this.viagem.getOs() + " adicionado", "");
		}else{
			this.viagemService.saveOrUpdate(viagem);
			FacesMessageUtil.addInfoContextFacesMessage("Viagem OS n� " + this.viagem.getOs() + " atualizado", "");
		}
		
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		request.getSession().setAttribute("osViagem", viagem.getOs());
		
		limpar();
		
		return "pretty:pesquisarViagem";
	}
	
	public void remover(Viagem viagem){
		this.viagemService.delete(viagem);
		FacesMessageUtil.addInfoContextFacesMessage("Viagem exclu�da com sucesso", "");
	}
	
	public void cancelar(){
		this.viagem.setStatus(Status.CANCELADA);
		this.viagem.setDataCancelamento(LocalDateTime.now());
		this.viagemService.saveOrUpdate(viagem);
		FacesMessageUtil.addInfoContextFacesMessage("Viagem cancelada com sucesso", "");
	}
	
	@URLAction(mappingId = "solicitarVoltaViagem", onPostback = false)
	public void solicitarViagemDeVolta() {
		this.viagem = viagemService.findById(this.getId());
		 
		try {
			Viagem viagemCloned = (Viagem) viagem.clone();
			viagemCloned.setViagemIda(this.viagem);
			this.viagem = viagemCloned;
			this.viagem.setTipoViagem(TipoViagem.VOLTA);
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
	}
	
	@Inject
	private VisualizadorImpressaoManagedBean visualizadorImpressaoManagedBean;
	
	@URLAction(mappingId = "impressaoViagem", onPostback = false)
	public void imprimir() throws ApplicationErrorException{
		this.viagem = viagemService.findById(id);
		
		visualizadorImpressaoManagedBean.exibirViagem(viagem);
	}
	
	public List<Municipio> getListaMunicipios(){
		if (ufOrigem == null)
			return null;
		
		return municipioService.findAllByUF(this.ufOrigem);
	}
	
	public List<CoordenadoriaDemandante> getListaCoordenadoriaDemandante(){
		return this.coordenadoriaDemandanteService.findAllAtivos();
	}
	
	public Viagem getViagem() {
		return viagem;
	}

	public void setViagem(Viagem viagem) {
		this.viagem = viagem;
	}

	public boolean isEditando() {
		return editando;
	}

	public void setEditando(boolean editando) {
		this.editando = editando;
	}

	public LazyObjectDataModel<Viagem> getListaViagem() {
		return listaViagem;
	}

	public void setListaViagem(LazyObjectDataModel<Viagem> listaViagem) {
		this.listaViagem = listaViagem;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ViagemDTO getViagemDTO() {
		return viagemDTO;
	}

	public void setViagemDTO(ViagemDTO viagemDTO) {
		this.viagemDTO = viagemDTO;
	}

	public List<Servidor> getListaServidores() {
		return listaServidores;
	}

	public void setListaServidores(List<Servidor> listaServidores) {
		this.listaServidores = listaServidores;
	}

	public UF getUfOrigem() {
		return ufOrigem;
	}

	public void setUfOrigem(UF ufOrigem) {
		this.ufOrigem = ufOrigem;
	}

	public UF getUfDestino() {
		return ufDestino;
	}

	public void setUfDestino(UF ufDestino) {
		this.ufDestino = ufDestino;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

}
