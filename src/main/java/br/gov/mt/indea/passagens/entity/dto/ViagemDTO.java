package br.gov.mt.indea.passagens.entity.dto;

import java.io.Serializable;
import java.util.List;

import org.springframework.util.ObjectUtils;

import br.gov.mt.indea.passagens.entity.passagens.CoordenadoriaDemandante;
import br.gov.mt.indea.passagens.enums.Dominio.Status;

public class ViagemDTO implements AbstractDTO, Serializable {

	private static final long serialVersionUID = -6902637182148275565L;
	
	private String os;
	
	private Status status;
	
	private List<CoordenadoriaDemandante> listaCoordenadoriaDemandante;
	
	private CoordenadoriaDemandante coordenadoriaDemandante;

	public boolean isNull(){
		return ObjectUtils.isEmpty(this);
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public List<CoordenadoriaDemandante> getListaCoordenadoriaDemandante() {
		return listaCoordenadoriaDemandante;
	}

	public void setListaCoordenadoriaDemandante(List<CoordenadoriaDemandante> listaCoordenadoriaDemandante) {
		this.listaCoordenadoriaDemandante = listaCoordenadoriaDemandante;
	}

	public CoordenadoriaDemandante getCoordenadoriaDemandante() {
		return coordenadoriaDemandante;
	}

	public void setCoordenadoriaDemandante(CoordenadoriaDemandante coordenadoriaDemandante) {
		this.coordenadoriaDemandante = coordenadoriaDemandante;
	}

}
