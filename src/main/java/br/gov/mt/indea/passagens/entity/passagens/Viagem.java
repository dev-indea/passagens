package br.gov.mt.indea.passagens.entity.passagens;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.passagens.enums.Dominio.Status;
import br.gov.mt.indea.passagens.enums.Dominio.TipoViagem;

@Audited
@Entity
public class Viagem extends BaseEntity<Long> implements Serializable, Cloneable{

	private static final long serialVersionUID = 1517933660655001452L;

	@Id
	@SequenceGenerator(name="viagem_seq", sequenceName="viagem_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="viagem_seq")
	private Long id;
	
	@Column(name="data_cadastro")
	@NotNull
	private LocalDateTime dataCadastro;
	
	private String os;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="id_municipio_origem")
	private Municipio municipioOrigem;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="id_municipio_destino")
	private Municipio municipioDestino;
	
	@Column(name = "observacao_ida", length = 500)
	private String observacao;
	
	@Column(name = "empresa_ida", length = 255)
	private String empresa;
	
	@ManyToOne(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, fetch = FetchType.EAGER)
	@JoinColumn(name = "id_servidor")
	private Servidor servidor = new Servidor();
	
	@Column(name="motivo", length = 1000)
	private String motivo;
	
	@Column(name="data_inicio")
	private LocalDateTime dataInicio;
	
	@Enumerated(EnumType.STRING)
	@Column(name="status")
	private Status status = Status.NOVA;
	
	@Enumerated(EnumType.STRING)
	@Column(name="tipo_viagem")
	private TipoViagem tipoViagem = TipoViagem.IDA;
	
	@ManyToOne(cascade = {CascadeType.DETACH, CascadeType.REFRESH}, fetch = FetchType.EAGER)
	@JoinColumn(name = "id_usuario_autorizador")
	private Usuario usuarioAutorizador;
	
	@Column(name="data_autorizacao")
	private LocalDateTime dataAutorizacao;
	
	@ManyToOne(cascade = {CascadeType.DETACH, CascadeType.REFRESH}, fetch = FetchType.EAGER)
	@JoinColumn(name="id_fonte_pagamento")
	private FontePagamento fontePagamento;
	
	@ManyToOne(cascade = {CascadeType.DETACH, CascadeType.REFRESH}, fetch = FetchType.EAGER)
	@JoinColumn(name = "id_usuario_aprovacao")
	private Usuario usuarioAprovacao;
	
	@Column(name="data_aprovacao")
	private LocalDateTime dataAprovacao;
	
	@ManyToOne(cascade={CascadeType.DETACH, CascadeType.REFRESH}, fetch=FetchType.LAZY)
	@JoinColumn(name = "id_coordenadoria_demandante")
	private CoordenadoriaDemandante coordenadoriaDemandante;
	
	@Column(name = "assinatura_eletronica")
	private String assinaturaEletronica;
	
	@Column(name="data_cancelamento")
	private LocalDateTime dataCancelamento;
	
	@Column(name="motivo_cancelamento", length = 500)
	private LocalDateTime motivoCancelamento;
	
//	@OneToMany(mappedBy="viagem", orphanRemoval=true, cascade=CascadeType.ALL, fetch=FetchType.LAZY)
//	private List<Trecho> trechos = new ArrayList<Trecho>();
	
	@OneToOne(fetch=FetchType.LAZY, cascade = {CascadeType.ALL})
	@JoinColumn(name="id_viagem_anexo")
	private ViagemAnexo viagemAnexo;
	
	@OneToOne(fetch=FetchType.LAZY, cascade = {CascadeType.ALL})
	@JoinColumn(name="id_viagem_ida")
	private Viagem viagemIda;
	
	@Column(name = "justificativa_trecho_diverg", length = 1000)
	private String justificativaTrechoDivergente;
	
	public Viagem() {
		
	}
	
	public Viagem(Servidor servidor) {
		this.servidor = servidor;
	}
	
	public boolean isViagemNova() {
		return this.status.equals(Status.NOVA);
	}
	
	public boolean isViagemEmitida() {
		return this.status.equals(Status.EMITIDA);
	}
	
	public boolean viagemPodeSerCancelada(){
		if (this.status.equals(Status.NOVA) && this.status.equals(Status.AUTORIZADA_COORDENADORIA) )
			return true;
		return false;
	}
	
	public boolean isTrechoIgualViagemIda() {
		boolean isOk = true;
		
		if (this.getTipoViagem() != null && this.getTipoViagem().equals(TipoViagem.VOLTA))
			if (this.viagemIda != null) {
				if (this.municipioDestino != null)
					if (!this.municipioDestino.equals(this.viagemIda.getMunicipioOrigem()))
						isOk = false;
				
				if (this.municipioOrigem != null)
					if (!this.municipioOrigem.equals(this.viagemIda.getMunicipioDestino()))
						isOk = false;
			}
		
		return isOk;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDateTime getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(LocalDateTime dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public Municipio getMunicipioOrigem() {
		return municipioOrigem;
	}

	public void setMunicipioOrigem(Municipio municipioOrigem) {
		this.municipioOrigem = municipioOrigem;
	}

	public Municipio getMunicipioDestino() {
		return municipioDestino;
	}

	public void setMunicipioDestino(Municipio municipioDestino) {
		this.municipioDestino = municipioDestino;
	}

	public Servidor getServidor() {
		return servidor;
	}

	public void setServidor(Servidor servidor) {
		this.servidor = servidor;
	}

	public LocalDateTime getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(LocalDateTime dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Usuario getUsuarioAutorizador() {
		return usuarioAutorizador;
	}

	public void setUsuarioAutorizador(Usuario usuarioAutorizador) {
		this.usuarioAutorizador = usuarioAutorizador;
	}

	public LocalDateTime getDataAutorizacao() {
		return dataAutorizacao;
	}

	public void setDataAutorizacao(LocalDateTime dataAutorizacao) {
		this.dataAutorizacao = dataAutorizacao;
	}

	public CoordenadoriaDemandante getCoordenadoriaDemandante() {
		return coordenadoriaDemandante;
	}

	public void setCoordenadoriaDemandante(CoordenadoriaDemandante coordenadoriaDemandante) {
		this.coordenadoriaDemandante = coordenadoriaDemandante;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Viagem other = (Viagem) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Usuario getUsuarioAprovacao() {
		return usuarioAprovacao;
	}

	public void setUsuarioAprovacao(Usuario usuarioAprovacao) {
		this.usuarioAprovacao = usuarioAprovacao;
	}

	public LocalDateTime getDataAprovacao() {
		return dataAprovacao;
	}
 
	public void setDataAprovacao(LocalDateTime dataAprovacao) {
		this.dataAprovacao = dataAprovacao;
	}

	public FontePagamento getFontePagamento() {
		return fontePagamento;
	}

	public void setFontePagamento(FontePagamento fontePagamento) {
		this.fontePagamento = fontePagamento;
	}

	public String getAssinaturaEletronica() {
		return assinaturaEletronica;
	}

	public void setAssinaturaEletronica(String assinaturaEletronica) {
		this.assinaturaEletronica = assinaturaEletronica;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public String getEmpresa() {
		return empresa;
	}

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	public LocalDateTime getDataCancelamento() {
		return dataCancelamento;
	}

	public void setDataCancelamento(LocalDateTime dataCancelamento) {
		this.dataCancelamento = dataCancelamento;
	}

	public LocalDateTime getMotivoCancelamento() {
		return motivoCancelamento;
	}

	public void setMotivoCancelamento(LocalDateTime motivoCancelamento) {
		this.motivoCancelamento = motivoCancelamento;
	}
//
//	public List<Trecho> getTrechos() {
//		return trechos;
//	}
//
//	public void setTrechos(List<Trecho> trechos) {
//		this.trechos = trechos;
//	}
	
	public ViagemAnexo getViagemAnexo() {
		return viagemAnexo;
	}

	public void setViagemAnexo(ViagemAnexo viagemAnexo) {
		this.viagemAnexo = viagemAnexo;
	}

	public TipoViagem getTipoViagem() {
		return tipoViagem;
	}

	public void setTipoViagem(TipoViagem tipoViagem) {
		this.tipoViagem = tipoViagem;
	}

	public Viagem getViagemIda() {
		return viagemIda;
	}

	public void setViagemIda(Viagem viagemIda) {
		this.viagemIda = viagemIda;
	}

	public String getJustificativaTrechoDivergente() {
		return justificativaTrechoDivergente;
	}

	public void setJustificativaTrechoDivergente(String justificativaTrechoDivergente) {
		this.justificativaTrechoDivergente = justificativaTrechoDivergente;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		Viagem clone = (Viagem) super.clone();
		Municipio municipioOrigem = clone.getMunicipioOrigem();
		
		clone.setMunicipioOrigem(clone.getMunicipioDestino());
		clone.setMunicipioDestino(municipioOrigem);
		
		clone.setStatus(null);
		clone.setId(null);
		clone.setDataCadastro(null);
		clone.setDataAprovacao(null);
		clone.setDataAutorizacao(null);
		clone.setDataCancelamento(null);
		clone.setDataInicio(null);
		clone.setAssinaturaEletronica(null);
		clone.setMotivo(null);
		clone.setMotivoCancelamento(null);
		clone.setViagemAnexo(null);
		clone.setUsuarioAprovacao(null);
		clone.setUsuarioAutorizador(null);
		
		return clone;
	}
	
}
