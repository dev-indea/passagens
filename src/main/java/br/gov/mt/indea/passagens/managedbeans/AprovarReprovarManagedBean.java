package br.gov.mt.indea.passagens.managedbeans;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.passagens.entity.dbindea.Servidor;
import br.gov.mt.indea.passagens.entity.dto.ViagemDTO;
import br.gov.mt.indea.passagens.entity.passagens.CoordenadoriaDemandante;
import br.gov.mt.indea.passagens.entity.passagens.FontePagamento;
import br.gov.mt.indea.passagens.entity.passagens.Viagem;
import br.gov.mt.indea.passagens.enums.Dominio.Status;
import br.gov.mt.indea.passagens.security.UserSecurity;
import br.gov.mt.indea.passagens.service.CoordenadoriaDemandanteService;
import br.gov.mt.indea.passagens.service.FontePagamentoService;
import br.gov.mt.indea.passagens.service.LazyObjectDataModel;
import br.gov.mt.indea.passagens.service.ServidorService;
import br.gov.mt.indea.passagens.service.ViagemService;
import br.gov.mt.indea.passagens.util.FacesMessageUtil;

@Named
@ViewScoped
@URLBeanName("aprovarReprovarManagedBean")
@URLMappings(mappings = {
		@URLMapping(id = "pesquisarAprovarReprovar", pattern = "/aprovarreprovar/pesquisar", viewId = "/pages/aprovarreprovar/lista.jsf"),
		@URLMapping(id = "permissoesAprovarReprovar", pattern = "/aprovarreprovar/#{aprovarReprovarManagedBean.id}/permissoes", viewId = "/pages/aprovarreprovar/permissoes.jsf")})
public class AprovarReprovarManagedBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2604643337140244881L;

	private LazyObjectDataModel<Viagem> listaViagem;
	
	private List<Servidor> listaServidores;
	
	private List<FontePagamento> listaFontePagamentos;
	
	private String motivo;
	
	@Inject
	private Viagem viagemSelecionada;
	
	private Long viagemSelecionadaId;

	@Inject
	private ViagemDTO viagemDTO;
	
	@Inject
	private ViagemService viagemService;
	
	@Inject
	private ServidorService servidorService;
	
	@Inject
	private FontePagamentoService fontePagamentoService;
	
	@Inject
	private CoordenadoriaDemandanteService coordenadoriaDemandanteService;
	
	@Inject
	private UserSecurity userSecurity;
	
	private boolean editando;
	
	private Long id;
	
	@PostConstruct
	private void init(){
		listaFontePagamentos = fontePagamentoService.findAll();
		
	}
	
	public void limpar(){
	}
	
	public void buscarViagens(){
		if (this.viagemDTO.getCoordenadoriaDemandante() == null)
			this.viagemDTO.setListaCoordenadoriaDemandante(this.getListaCoordenadoriaDemandante());
		else
			this.viagemDTO.setListaCoordenadoriaDemandante(null);
		
		this.listaViagem = new LazyObjectDataModel<Viagem>(this.viagemService, this.viagemDTO);
		
	}
	
	public List<Servidor> buscarServidor(String query){
		
		listaServidores = servidorService.findByNome(query);
		return listaServidores;
	}

	@URLAction(mappingId = "pesquisarAprovarReprovar", onPostback = false)
	public void pesquisar(){
		limpar();

		this.viagemDTO = new ViagemDTO();
		this.viagemDTO.setStatus(Status.NOVA);
		this.buscarViagens();

	}
	
	public void aprovar() {
		this.viagemSelecionada.setStatus(Status.AUTORIZADA_COORDENADORIA);
		this.viagemSelecionada.setDataAprovacao(LocalDateTime.now());
		this.viagemSelecionada.setUsuarioAprovacao(userSecurity.getUsuario());
		this.viagemSelecionada.getFontePagamento();
		viagemService.saveOrUpdate(this.viagemSelecionada);
		
		FacesMessageUtil.addInfoContextFacesMessage("Passagem autorizada com sucesso", "");
	}
	
	public void reprovar() {
		this.viagemSelecionada.setStatus(Status.NAO_AUTORIZADA_COORDENADORIA);
		this.viagemSelecionada.setDataAprovacao(LocalDateTime.now());
		this.viagemSelecionada.setUsuarioAprovacao(userSecurity.getUsuario());
		viagemService.saveOrUpdate(this.viagemSelecionada);
		
		FacesMessageUtil.addInfoContextFacesMessage("Passagem reprovada com sucesso", "");
	}

	public void abrirTelaAprovarViagem(Viagem viagem) {
		this.setViagemSelecionada(viagemService.findById(viagem.getId()));
	}
	
	public void abrirTelaReprovarViagem(Viagem viagem) {
		this.setViagemSelecionada(viagem);
	}
	
	public List<CoordenadoriaDemandante> getListaCoordenadoriaDemandante(){
		return this.coordenadoriaDemandanteService.findAllAtivosBy(this.userSecurity.getUsuario());
	}

	public LazyObjectDataModel<Viagem> getListaViagem() {
		return listaViagem;
	}

	public void setListaViagem(LazyObjectDataModel<Viagem> listaViagem) {
		this.listaViagem = listaViagem;
	}

	public Viagem getViagemSelecionada() {
		return viagemSelecionada;
	}

	public void setViagemSelecionada(Viagem viagem) {
		this.viagemSelecionada = viagem;
	}

	public ViagemService getViagemService() {
		return viagemService;
	}

	public void setViagemService(ViagemService viagemService) {
		this.viagemService = viagemService;
	}

	public ViagemDTO getViagemDTO() {
		return viagemDTO;
	}

	public void setViagemDTO(ViagemDTO viagemDTO) {
		this.viagemDTO = viagemDTO;
	}

	public boolean isEditando() {
		return editando;
	}

	public void setEditando(boolean editando) {
		this.editando = editando;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Servidor> getListaServidores() {
		return listaServidores;
	}

	public void setListaServidores(List<Servidor> listaServidores) {
		this.listaServidores = listaServidores;
	}

	public List<FontePagamento> getListaFontePagamentos() {
		return listaFontePagamentos;
	}

	public void setListaFontePagamentos(List<FontePagamento> listaFontePagamentos) {
		this.listaFontePagamentos = listaFontePagamentos;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public Long getViagemSelecionadaId() {
		return viagemSelecionadaId;
	}

	public void setViagemSelecionadaId(Long viagemSelecionadaId) {
		this.viagemSelecionadaId = viagemSelecionadaId;
	}
}
