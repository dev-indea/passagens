package br.gov.mt.indea.passagens.exception;

@javax.ejb.ApplicationException(rollback=true)
public class ApplicationRuntimeException extends RuntimeException {

	private static final long serialVersionUID = -5849615990643923188L;
	
	private String mensagem;
	
	private Throwable throwable;
	
	public ApplicationRuntimeException(){
		super();
	}
	
	public ApplicationRuntimeException(String mensagem){
		super(mensagem);
		this.mensagem = mensagem;
	}
	
	public ApplicationRuntimeException(Throwable throwable){
		super(throwable);
		this.throwable = throwable;
	}
	
	public ApplicationRuntimeException(String mensagem, Throwable throwable){
		super(mensagem, throwable);
		this.mensagem = mensagem;
		this.throwable = throwable;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public Throwable getThrowable() {
		return throwable;
	}

	public void setThrowable(Throwable throwable) {
		this.throwable = throwable;
	}

}
