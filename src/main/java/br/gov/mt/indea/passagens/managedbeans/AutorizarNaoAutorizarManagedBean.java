package br.gov.mt.indea.passagens.managedbeans;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.passagens.entity.dbindea.Servidor;
import br.gov.mt.indea.passagens.entity.dto.ViagemDTO;
import br.gov.mt.indea.passagens.entity.passagens.Autorizador;
import br.gov.mt.indea.passagens.entity.passagens.Contrato;
import br.gov.mt.indea.passagens.entity.passagens.Viagem;
import br.gov.mt.indea.passagens.enums.Dominio.Status;
import br.gov.mt.indea.passagens.security.UserSecurity;
import br.gov.mt.indea.passagens.service.AutorizadorService;
import br.gov.mt.indea.passagens.service.ContratoService;
import br.gov.mt.indea.passagens.service.GeradorAssinaturaVirtual;
import br.gov.mt.indea.passagens.service.LazyObjectDataModel;
import br.gov.mt.indea.passagens.service.ServidorService;
import br.gov.mt.indea.passagens.service.ViagemService;
import br.gov.mt.indea.passagens.util.Email;
import br.gov.mt.indea.passagens.util.FacesMessageUtil;

@Named
@ViewScoped
@URLBeanName("autorizarNaoAutorizarManagedBean")
@URLMappings(mappings = {
		@URLMapping(id = "pesquisarAutorizarNaoAutorizar", pattern = "/autorizarnaoautorizar/pesquisar", viewId = "/pages/autorizarnaoautorizar/lista.jsf"),
		@URLMapping(id = "permissoesAutorizarNaoAutorizar", pattern = "/autorizarnaoautorizar/#{autorizarNaoAutorizarManagedBean.id}/permissoes", viewId = "/pages/autorizarnaoautorizar/permissoes.jsf")})
public class AutorizarNaoAutorizarManagedBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2604643337140244881L;

	private LazyObjectDataModel<Viagem> listaViagem;
	
	private List<Servidor> listaServidores;
	
	private List<Autorizador> listaAutorizadores;
	
	private String motivo;
	
	@Inject
	private Viagem viagemSelecionada;
	
	private Long viagemSelecionadaId;

	@Inject
	private ViagemDTO viagemDTO;
	
	@Inject
	private ViagemService viagemService;
	
	@Inject
	private ServidorService servidorService;
	
	@Inject
	private AutorizadorService autorizadorService;
	
	@Inject
	private ContratoService contratoService;
	
	@Inject
	private UserSecurity userSecurity;
	
	private boolean editando;
	
	private Long id;
	
	@PostConstruct
	private void init(){
		//listaFontePagamentos = fontePagamentoService.findAll();
		this.listaAutorizadores = autorizadorService.findAll();
	}
	
	public void limpar(){
	}
	
	public void buscarViagens(){
		this.listaViagem = new LazyObjectDataModel<Viagem>(this.viagemService, this.viagemDTO);
	}
	
	public List<Servidor> buscarServidor(String query){
		
		listaServidores = servidorService.findByNome(query);
		return listaServidores;
	}

	@URLAction(mappingId = "pesquisarAutorizarNaoAutorizar", onPostback = false)
	public void pesquisar(){
		limpar();

		this.viagemDTO = new ViagemDTO();
		this.viagemDTO.setStatus(Status.AUTORIZADA_COORDENADORIA);
		this.buscarViagens();

	}
	
	public void autorizar() {
		viagemSelecionada = viagemService.findByIdFetchAll(viagemSelecionada.getId());
		Contrato contrato = contratoService.getInstance();
		
		String assinatura = GeradorAssinaturaVirtual.gerarAssinatura(viagemSelecionada);
		viagemSelecionada.setAssinaturaEletronica(assinatura);
		
		this.viagemSelecionada.setStatus(Status.AUTORIZADA_COPAS);
		this.viagemSelecionada.setDataAutorizacao(LocalDateTime.now());
		this.viagemSelecionada.setUsuarioAutorizador(userSecurity.getUsuario());
		viagemService.saveOrUpdate(this.viagemSelecionada);
		
		FacesMessageUtil.addInfoContextFacesMessage("Passagem autorizada com sucesso", "");
		Email.solicitarPassagemEmpresa(viagemSelecionada, contrato.getEmail(), true);
	}

	public void naoAutorizar() {
		this.viagemSelecionada.setStatus(Status.NAO_AUTORIZADA_COPAS);
		this.viagemSelecionada.setDataAutorizacao(LocalDateTime.now());
		this.viagemSelecionada.setUsuarioAutorizador(userSecurity.getUsuario());
		viagemService.saveOrUpdate(this.viagemSelecionada);
		
		FacesMessageUtil.addInfoContextFacesMessage("Passagem reprovada com sucesso", "");
	}

	public void abrirTelaAutorizarViagem(Viagem viagem) {
		this.setViagemSelecionada(viagemService.findById(viagem.getId()));
	}
	
	public void abrirTelaNaoAutorizarViagem(Viagem viagem) {
		this.setViagemSelecionada(viagem);
	}
	
	public boolean isUsuarioLogadoAutorizador() {
		for (Autorizador autorizador : listaAutorizadores) {
			if (autorizador.getUsuario().equals(userSecurity.getUsuario()))
				return true;
		}
		
		return false;
	}

	public LazyObjectDataModel<Viagem> getListaViagem() {
		return listaViagem;
	}

	public void setListaViagem(LazyObjectDataModel<Viagem> listaViagem) {
		this.listaViagem = listaViagem;
	}

	public Viagem getViagemSelecionada() {
		return viagemSelecionada;
	}

	public void setViagemSelecionada(Viagem viagem) {
		this.viagemSelecionada = viagem;
	}

	public ViagemService getViagemService() {
		return viagemService;
	}

	public void setViagemService(ViagemService viagemService) {
		this.viagemService = viagemService;
	}

	public ViagemDTO getViagemDTO() {
		return viagemDTO;
	}

	public void setViagemDTO(ViagemDTO viagemDTO) {
		this.viagemDTO = viagemDTO;
	}

	public boolean isEditando() {
		return editando;
	}

	public void setEditando(boolean editando) {
		this.editando = editando;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Servidor> getListaServidores() {
		return listaServidores;
	}

	public void setListaServidores(List<Servidor> listaServidores) {
		this.listaServidores = listaServidores;
	}


	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public Long getViagemSelecionadaId() {
		return viagemSelecionadaId;
	}

	public void setViagemSelecionadaId(Long viagemSelecionadaId) {
		this.viagemSelecionadaId = viagemSelecionadaId;
	}
}
