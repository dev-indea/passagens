package br.gov.mt.indea.passagens.entity.passagens;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;

@Entity
@DiscriminatorValue("SERVIDOR")
@Audited
public class Servidor extends Pessoa {

	private static final long serialVersionUID = 7811664327889819479L;

	@Size(max = 10)
	@Column(length = 10)
	private String matricula;

	@Transient
	private br.gov.mt.indea.passagens.entity.dbindea.Servidor servidorIndeaWeb;

	@Column(name = "id_servidor")
	private Long idServidor;

	public Long getIdServidor() {
		return idServidor;
	}

	public String getNome() {
		return super.getNome();
	}

	public String getCPf() {
		return super.getCpf();
	}

	public String getEmail() {
		return super.getEmail();
	}

	public String getCargo() {
		if (this.servidorIndeaWeb != null)
			return this.servidorIndeaWeb.getCargo().getNome();
		return null;
	}

	public String getMatricula() {
		if (this.servidorIndeaWeb != null)
			return this.servidorIndeaWeb.getMatricula();
		return null;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public void setServidorIndeaWeb(br.gov.mt.indea.passagens.entity.dbindea.Servidor servidorIndeaWeb) {
		this.servidorIndeaWeb = servidorIndeaWeb;
		this.setIdServidor(servidorIndeaWeb.getId());
		this.setNome(servidorIndeaWeb.getNome());
		this.setCpf(servidorIndeaWeb.getCpf());
		this.setEmail(servidorIndeaWeb.getEmail());
	}

	public br.gov.mt.indea.passagens.entity.dbindea.Servidor getServidorIndeaWeb() {
		return servidorIndeaWeb;
	}

	private void setIdServidor(Long idServidor) {
		this.idServidor = idServidor;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((super.getCpf() == null) ? 0 : super.getCpf().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Servidor other = (Servidor) obj;
		if (super.getCpf() == null) {
			if (other.getCpf() != null)
				return false;
		} else if (!getCpf().equals(other.getCpf()))
			return false;
		return true;
	}

}
