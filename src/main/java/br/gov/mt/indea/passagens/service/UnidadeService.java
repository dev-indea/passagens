package br.gov.mt.indea.passagens.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.hibernate.Query;

import br.gov.mt.indea.passagens.entity.dbindea.Unidade;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class UnidadeService extends PaginableService<Unidade, Long> {

	protected UnidadeService() {
		super(Unidade.class);
	}

	@SuppressWarnings("unchecked")
	public List<Unidade> findAllOrderByNome() {
		List<Unidade> listaUnidade = null;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select unidade")
		   .append("  from Unidade unidade ")
		   .append("  left join fetch unidade.unidadePai ")
		   .append("  left join fetch unidade.municipio ")
		   .append(" order by unidade.nome");
		
		Query query = getSession().createQuery(sql.toString());
		listaUnidade = query.list();
		
		return listaUnidade;
	}
	
	@Override
	public void validar(Unidade model) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void validarPersist(Unidade model) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void validarMerge(Unidade model) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void validarDelete(Unidade model) {
		// TODO Auto-generated method stub
		
	}

}
