package br.gov.mt.indea.passagens.managedbeans;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletResponse;

import br.gov.mt.indea.passagens.entity.passagens.Contrato;
import br.gov.mt.indea.passagens.entity.passagens.Viagem;
import br.gov.mt.indea.passagens.exception.ApplicationErrorException;
import br.gov.mt.indea.passagens.service.ContratoService;
import br.gov.mt.indea.passagens.util.GeradorDeRelatorio;
import net.sf.jasperreports.engine.JRException;

@Named
public class VisualizadorImpressaoManagedBean implements Serializable{
	
	private static final long serialVersionUID = 6547144677010351527L;
	
	@Inject
	ContratoService contratoService;

	public void exibirViagem(Viagem viagem) throws ApplicationErrorException{
		List<Viagem> lista = new ArrayList<Viagem>();
		lista.add(viagem);
		
		Contrato contrato = contratoService.getInstance();
		
		try {
			
			byte[] pdf = GeradorDeRelatorio.gerarFormularioSolicitacaoDePassagemTerrestre(lista, contrato);
			
			FacesContext fc = FacesContext.getCurrentInstance();
			HttpServletResponse response = (HttpServletResponse) fc.getExternalContext().getResponse();
			response.reset();
			response.setContentLength(pdf.length);
			response.setHeader("Content-disposition", "inline; filename=" + viagem.getMunicipioOrigem().getNome() + " - " + viagem.getMunicipioDestino().getNome() + " - " + viagem.getDataInicio().format(java.time.format.DateTimeFormatter.ofPattern("dd/MM/yyyy")) + ".pdf");
			 
			response.getOutputStream().write(pdf);
			response.getOutputStream().flush();
			fc.responseComplete();
			
		} catch (JRException e) {
			throw new ApplicationErrorException("Erro ao gerar o pdf", e);
		} catch (IOException e) {
			throw new ApplicationErrorException("Erro ao gerar o pdf", e);
		}
	}

}
