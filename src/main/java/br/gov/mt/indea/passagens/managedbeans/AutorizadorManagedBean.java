package br.gov.mt.indea.passagens.managedbeans;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.passagens.entity.passagens.Autorizador;
import br.gov.mt.indea.passagens.entity.passagens.Usuario;
import br.gov.mt.indea.passagens.service.AutorizadorService;
import br.gov.mt.indea.passagens.service.UsuarioService;
import br.gov.mt.indea.passagens.util.FacesMessageUtil;

@Named
@ViewScoped
@URLBeanName("autorizadorManagedBean")
@URLMappings(mappings = {
		@URLMapping(id = "pesquisarAutorizador", pattern = "/autorizador/pesquisar", viewId = "/pages/autorizador/lista.jsf"),
		@URLMapping(id = "incluirAutorizador", pattern = "/autorizador/incluir", viewId = "/pages/autorizador/novo.jsf"),
		@URLMapping(id = "alterarAutorizador", pattern = "/autorizador/alterar/#{autorizadorManagedBean.id}", viewId = "/pages/autorizador/novo.jsf"),
		@URLMapping(id = "visualizarAutorizador", pattern = "/autorizador/visualizar/#{autorizadorManagedBean.id}", viewId = "/pages/autorizador/novo.jsf")})
public class AutorizadorManagedBean implements Serializable {

	private static final long serialVersionUID = 2345719080339499530L;

	private boolean visualizando = false;
	
	private boolean editando = false;
	
	private Long id;
	
	private List<Autorizador> listaAutorizador;
	
	@Inject
	private Autorizador autorizador;
	
	@Inject
	private AutorizadorService autorizadorService;
	
	@Inject
	private UsuarioService usuarioService;
	
	@PostConstruct
	private void init(){
	}
	
	public void limpar(){
		this.autorizador = new Autorizador();
	}
	
	public void buscarAutorizadors() {
		this.listaAutorizador = autorizadorService.findAll();
	}
	
	public List<Usuario> getListaUsuarios(){
		return this.usuarioService.findAllByNome();
	}
	
	@URLAction(mappingId = "pesquisarAutorizador", onPostback = false)
	public void pesquisar(){
		limpar();
		
		this.buscarAutorizadors();
	}
	
	@URLAction(mappingId = "visualizarAutorizador", onPostback = false)
	public void visualizar(){
		this.visualizando = true;
		this.editando = false;
	}
	
	@URLAction(mappingId = "alterarAutorizador", onPostback = false)
	public void editar(){
		this.autorizador = autorizadorService.findById(this.getId());
		
		this.visualizando = false;
		this.editando = true;
	}
	
	public String adicionar() {
		autorizadorService.saveOrUpdate(this.autorizador);
		
		return "pretty:pesquisarAutorizador";
	}
	
	public void atualizarStatus(Autorizador autorizador) {
		autorizadorService.saveOrUpdate(autorizador);
		
		this.buscarAutorizadors();
	}
	
	public void remover(Autorizador autorizador) {
		this.autorizadorService.delete(autorizador);
		
		this.buscarAutorizadors();
		
		FacesMessageUtil.addInfoContextFacesMessage("Autorizador n� " + autorizador.getUsuario().getNome() + " exclu�do com sucesso", "");
	}
	
	public boolean isVisualizando() {
		return visualizando;
	}

	public void setVisualizando(boolean visualizando) {
		this.visualizando = visualizando;
	}

	public boolean isEditando() {
		return editando;
	}

	public void setEditando(boolean editando) {
		this.editando = editando;
	}

	public Autorizador getAutorizador() {
		return autorizador;
	}

	public void setAutorizador(Autorizador autorizador) {
		this.autorizador = autorizador;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Autorizador> getListaAutorizador() {
		return listaAutorizador;
	}

	public void setListaAutorizador(List<Autorizador> listaAutorizador) {
		this.listaAutorizador = listaAutorizador;
	}

}
