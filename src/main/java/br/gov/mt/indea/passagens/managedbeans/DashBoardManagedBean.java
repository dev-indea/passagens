package br.gov.mt.indea.passagens.managedbeans;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.passagens.entity.passagens.Trecho;
import br.gov.mt.indea.passagens.entity.passagens.Viagem;
import br.gov.mt.indea.passagens.entity.passagens.ViagemAnexo;
import br.gov.mt.indea.passagens.security.UserSecurity;
import br.gov.mt.indea.passagens.service.ViagemService;

@Named
@ViewScoped
@URLBeanName("dashBoardManagedBean")
@URLMappings(mappings = {
		@URLMapping(id = "dashboard", pattern = "/dashboard", viewId = "/dashboard.jsf")})
public class DashBoardManagedBean implements Serializable{

	private static final long serialVersionUID = 4902041813362871313L;

	@Inject
	UserSecurity userSecurity;

	List<Viagem> listaViagem;
	
	List<Trecho> listaTrecho;
	
	@Inject
	ViagemService viagemService;
	
	@PostConstruct
	private void init(){
		this.listaViagem = viagemService.findAllParaImpressao(userSecurity.getUsuario());
	}
	
	public StreamedContent downloadableFile(Viagem viagem) {
		ViagemAnexo viagemAnexo = viagemService.getViagemAnexo(viagem);
		
	    ByteArrayInputStream bais = new ByteArrayInputStream(viagemAnexo.getArquivo());
		StreamedContent file = new DefaultStreamedContent(bais, viagemAnexo.getTipoArquivo(), viagemAnexo.getNome());
	    return file;
	}
	
	public List<Viagem> getListaViagem() {
		return listaViagem;
	}

	public void setListaViagem(List<Viagem> listaViagem) {
		this.listaViagem = listaViagem;
	}

	public List<Trecho> getListaTrecho() {
		return listaTrecho;
	}

	public void setListaTrecho(List<Trecho> listaTrecho) {
		this.listaTrecho = listaTrecho;
	}
	
}