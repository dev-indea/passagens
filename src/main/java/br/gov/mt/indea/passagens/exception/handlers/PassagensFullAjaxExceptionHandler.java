package br.gov.mt.indea.passagens.exception.handlers;

import java.time.LocalDate;

import javax.faces.context.ExceptionHandler;
import javax.faces.context.FacesContext;

import org.omnifaces.exceptionhandler.FullAjaxExceptionHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.passagens.managedbeans.ErroManagedBean;
import br.gov.mt.indea.passagens.util.CDIServiceLocator;
import br.gov.mt.indea.passagens.util.TokenGenerator;

public class PassagensFullAjaxExceptionHandler extends FullAjaxExceptionHandler{
	
	private static final Logger log = LoggerFactory.getLogger(PassagensFullAjaxExceptionHandler.class);

	private ErroManagedBean erroManagedBean = CDIServiceLocator.getBean(ErroManagedBean.class);

	public PassagensFullAjaxExceptionHandler(ExceptionHandler wrapped) {
		super(wrapped);
	}
	
	@Override
	protected String findErrorPageLocation(FacesContext context, Throwable exception) {
		return "/erro.jsf";
	}
	
	@Override
	protected void logException(FacesContext context, Throwable exception, String location, LogReason reason) {
		LocalDate date = LocalDate.now();
		String token = TokenGenerator.csRandomAlphaNumericString(30);
		token = date.getDayOfMonth() + "" + String.format("%02d", date.getMonthValue()) + "" + date.getYear() + token;
		erroManagedBean.setCodigo(token);

		log.error("Erro desconhecido: {}", token, exception);
	}


}
