package br.gov.mt.indea.passagens.managedbeans;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.passagens.entity.passagens.Municipio;
import br.gov.mt.indea.passagens.entity.passagens.Trecho;
import br.gov.mt.indea.passagens.entity.passagens.UF;
import br.gov.mt.indea.passagens.entity.passagens.Viagem;
import br.gov.mt.indea.passagens.entity.passagens.ViagemAnexo;
import br.gov.mt.indea.passagens.enums.Dominio.Status;
import br.gov.mt.indea.passagens.service.MunicipioService;
import br.gov.mt.indea.passagens.service.ViagemService;

@Named
@ViewScoped
@URLBeanName("voucherManagedBean")
@URLMappings(mappings = {@URLMapping(id = "anexarVoucher", pattern = "/anexarVoucher/#{voucherManagedBean.token}", viewId = "/pages/viagem/voucher.jsf")})
public class VoucherManagedBean implements Serializable {

	private static final long serialVersionUID = 5401332308697941357L;
	
	@Inject
	private ViagemService viagemService;
	
	private Viagem viagem;
	
	private Trecho trecho;
	
	private String token;
	
	private UF ufViagemOrigem;
	
	private UF ufViagemDestino;
	
	private UF ufTrechoOrigem;
	
	private UF ufTrechoDestino;
	
	private UploadedFile file;
	
	private boolean finalizado = false;
	
	@Inject
	private MunicipioService municipioService;
	
	@PostConstruct
	private void init(){

	}
	
	public void limpar(){
	}
	
	@URLAction(mappingId = "anexarVoucher", onPostback = false)
	public void anexarVoucher(){
		viagem = viagemService.findByCodigoFetchALl(token);
		
		if (viagem != null) {
			this.ufViagemOrigem = viagem.getMunicipioOrigem().getUf();
			this.ufViagemDestino = viagem.getMunicipioDestino().getUf();
			
			ViagemAnexo viagemAnexo = viagemService.getViagemAnexo(viagem);
			if (viagemAnexo != null) {
				this.viagem.setViagemAnexo(viagemAnexo);
				this.finalizado = true;
			} else
				this.viagem.setViagemAnexo(new ViagemAnexo(this.viagem));
			
		}
	}
	
	public void editar(){
		
	}
	
	public String salvar() {
		
		if (this.viagem.getViagemAnexo().getArquivo() == null) {
			FacesContext context = FacesContext.getCurrentInstance();
			
			context.addMessage("cadastro:uploader", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Voucher: valor � obrigat�rio.", "Voucher: valor � obrigat�rio."));
			
			return "";
		}
		
		this.viagem.setStatus(Status.EMITIDA);
		viagemService.saveOrUpdate(this.viagem);
		this.finalizado = true;
		
		
		return "";
	}
	
	public void handleFileUpload(FileUploadEvent event) {
        byte[] file = new byte[event.getFile().getContents().length];
		System.arraycopy(event.getFile().getContents(),0,file,0,event.getFile().getContents().length);
		viagem.getViagemAnexo().setArquivo(file);
		viagem.getViagemAnexo().setNome(event.getFile().getFileName());
		viagem.getViagemAnexo().setTipoArquivo(event.getFile().getContentType());
    }
	
	public void removerVoucher() {
		viagem.getViagemAnexo().setArquivo(null);
		viagem.getViagemAnexo().setTipoArquivo(null);
	}
	
	public StreamedContent downloadableFile(Viagem viagem) {
		ViagemAnexo viagemAnexo = null;
		if (viagem.getViagemAnexo() == null)
			viagemAnexo = viagemService.getViagemAnexo(viagem);
		else
			viagemAnexo = viagem.getViagemAnexo();
		
	    ByteArrayInputStream bais = new ByteArrayInputStream(viagemAnexo.getArquivo());
		StreamedContent file = new DefaultStreamedContent(bais, viagemAnexo.getTipoArquivo(), viagemAnexo.getNome());
	    return file;
	}
	
	public List<Municipio> getListaMunicipios(){
		if (ufViagemOrigem == null)
			return null;
		
		return municipioService.findAllByUF(this.ufViagemOrigem);
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Viagem getViagem() {
		return viagem;
	}

	public void setViagem(Viagem viagem) {
		this.viagem = viagem;
	}

	public UploadedFile getFile() {
		return file;
	}

	public void setFile(UploadedFile file) {
		this.file = file;
	}

	public Trecho getTrecho() {
		return trecho;
	}

	public void setTrecho(Trecho trecho) {
		this.trecho = trecho;
	}

	public UF getUfViagemOrigem() {
		return ufViagemOrigem;
	}

	public void setUfViagemOrigem(UF ufViagemOrigem) {
		this.ufViagemOrigem = ufViagemOrigem;
	}

	public UF getUfViagemDestino() {
		return ufViagemDestino;
	}

	public void setUfViagemDestino(UF ufViagemDestino) {
		this.ufViagemDestino = ufViagemDestino;
	}

	public UF getUfTrechoOrigem() {
		return ufTrechoOrigem;
	}

	public void setUfTrechoOrigem(UF ufTrechoOrigem) {
		this.ufTrechoOrigem = ufTrechoOrigem;
	}

	public UF getUfTrechoDestino() {
		return ufTrechoDestino;
	}

	public void setUfTrechoDestino(UF ufTrechoDestino) {
		this.ufTrechoDestino = ufTrechoDestino;
	}

	public boolean isFinalizado() {
		return finalizado;
	}

	public void setFinalizado(boolean finalizado) {
		this.finalizado = finalizado;
	}
	
}
