package br.gov.mt.indea.passagens.entity.dbindea;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.gov.mt.indea.passagens.annotation.IndeaWeb;
import br.gov.mt.indea.passagens.entity.passagens.BaseEntity;

@Entity
@Table(name = "municipio")
@IndeaWeb
public class Municipio extends BaseEntity<Long> implements Serializable {

	private static final long serialVersionUID = 4117253577016540711L;

	@Id
    private Long id;
    
	@NotNull
    @Size(max = 35)
    @Column(nullable = false, length = 35)
    private String nome; 
    
	@NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "uf_id", nullable = false)
    private Uf uf;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Uf getUf() {
		return uf;
	}

	public void setUf(Uf uf) {
		this.uf = uf;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Municipio other = (Municipio) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}