package br.gov.mt.indea.passagens.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.passagens.entity.dto.AbstractDTO;
import br.gov.mt.indea.passagens.entity.dto.FontePagamentoDTO;
import br.gov.mt.indea.passagens.entity.passagens.FontePagamento;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class FontePagamentoService extends PaginableService<FontePagamento, Long> {

	private static final Logger log = LoggerFactory.getLogger(FontePagamento.class);
	
	protected FontePagamentoService() {
		super(FontePagamento.class);
	}

	public FontePagamento findById(Long id){
		FontePagamento fontepagamento;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select fontepagamento ")
		   .append("  from FontePagamento fontepagamento ")
		   .append(" where fontepagamento.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		fontepagamento = (FontePagamento) query.uniqueResult();
		
		return fontepagamento;
	}

	@Override
	public Long countAll(AbstractDTO dto) {
		StringBuilder sql = new StringBuilder();
		sql.append("select count(entity) ")
		   .append("  from " + this.getType().getSimpleName() + " as entity")
		   .append(" where 1 = 1");
		
		if (dto != null && !dto.isNull()){
			FontePagamentoDTO fontePagamentoDTO = (FontePagamentoDTO) dto;
			if(fontePagamentoDTO.getFonte()!=null) {
				sql.append(" AND entity.fonte = :fonte ");
			}
		}
		
		Query query = getSession().createQuery(sql.toString());
		
		if (dto != null && !dto.isNull()){
			FontePagamentoDTO fontePagamentoDTO = (FontePagamentoDTO) dto;
			if(fontePagamentoDTO.getFonte()!=null) {
				query.setParameter("fonte", fontePagamentoDTO.getFonte());
			}
		}
		
		return (Long) query.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<FontePagamento> findAllComPaginacao(AbstractDTO dto, int first, int pageSize, String sortField, String sortOrder) {
		StringBuilder sql = new StringBuilder();
		sql.append("select entity ")
		   .append("  from " + this.getType().getSimpleName() + " as entity")
		   .append(" where 1 = 1");
		
		if (dto != null && !dto.isNull()){
			FontePagamentoDTO fontePagamentoDTO = (FontePagamentoDTO) dto;
		
			if(fontePagamentoDTO.getFonte()!=null) {
				sql.append(" AND entity.fonte = :fonte ");
			}
		}
		
		if (StringUtils.isNotBlank(sortField))
			sql.append(" order by entity." + sortField + " " + sortOrder);

		Query query = getSession().createQuery(sql.toString());
		
		if (dto != null && !dto.isNull()){
			FontePagamentoDTO fontePagamentoDTO = (FontePagamentoDTO) dto;
			if(fontePagamentoDTO.getFonte()!=null) {
				query.setParameter("fonte", fontePagamentoDTO.getFonte());
			}
		}

		query.setFirstResult(first);
		query.setMaxResults(pageSize);

		List<FontePagamento> lista = query.list();
		
		return lista;
    }
	
	@Override
	public void saveOrUpdate(FontePagamento fontePagamento) {
		fontePagamento = (FontePagamento) this.getSession().merge(fontePagamento);
		
		super.saveOrUpdate(fontePagamento);
		
		log.info("Salvando Fonte Pagamento {}", fontePagamento.getId());
	}
	
	@Override
	public void delete(FontePagamento fontepagamento) {
		super.delete(fontepagamento);
		
		log.info("Removendo Fonte Pagamento {}", fontepagamento.getId());
	}
	
	@Override
	public void validar(FontePagamento model) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void validarPersist(FontePagamento model) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void validarMerge(FontePagamento model) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void validarDelete(FontePagamento model) {
		// TODO Auto-generated method stub
		
	}

}
