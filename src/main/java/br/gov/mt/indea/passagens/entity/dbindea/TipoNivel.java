package br.gov.mt.indea.passagens.entity.dbindea;

public enum TipoNivel {

    NAO_ESPECIFICADO("Não Especificado"),
    FUNDAMENTAL("Fundamental"),
    MEDIO("Médio"),
    SUPERIO("Superior");

    String descricao;

    TipoNivel(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }
}
