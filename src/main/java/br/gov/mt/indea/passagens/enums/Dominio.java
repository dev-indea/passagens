package br.gov.mt.indea.passagens.enums;

public class Dominio {
	
	public enum TipoUnidade {

		//ULE(1, "ULE", "Unidade Local de Execu��o"), URS(2, "URS", "Unidade Regional de Supervis�o");

		PRESIDENCIA(1,"Presidencia","Presidencia"),
	    DIRETORIA(2, "Diretoria","Diretoria"),
	    APOIO(3, "Unidade de Apoio", "Unidade de Apoio"),
	    COORDENADORIA(4, "Coordenadoria", "Coordenadoria"),
	    GERENCIA(5,"Gerência","Gerência"),
	    LABORATORIO(6,"Laborat�rio","Laborat�rio"),
	    URS(7,"URS","Unidade Regional de Supervis�o"),
	    ULE(8,"ULE","Unidade Local de Execu��o"),
	    PF(9,"Posto Fiscal","Posto Fiscal"),
	    PA(10,"Ponto de Atendimento","Ponto de Atendimento"),
	    BARREIRA(11,"Barreira","Barreira");
		
		private long value;
		private String descricao;
		private String longDescricao;

		TipoUnidade(long value, String descricao, String longDescricao) {
			this.value = value;
			this.descricao = descricao;
			this.longDescricao = longDescricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}

		public String getLongDescricao() {
			return longDescricao;
		}

		public void setLongDescricao(String longDescricao) {
			this.longDescricao = longDescricao;
		}
	}
	
	public enum TipoServidor {

		COLABORADOR_EVENTUAL("Colaborador Eventual"),
		INDEA("Indea"),
		PM("Pol�cia Militar");
		
		private String descricao;

		TipoServidor(String descricao) {
			this.descricao = descricao;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}

	}
	
	public enum AtivoInativo{
		ATIVO(1, "Ativo"),
		INATIVO(2, "Inativo");
		
		private long value;
		private String descricao;
		
		AtivoInativo (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum TipoPropriedade{
		ALDEIA_INDIGENA(1, "Aldeia Ind�gena"),
		ALOJAMENTO(2, "Alojamento"),
		ASSENTAMENTO(3, "Assentamento"),
		COMUNITARIO(4, "Comunit�rio"),
		CONFINAMENTO(5, "Confinamento"),
        HARAS(6, "Haras"),
		HOSPITAL_VETERINARIO(7, "Hospital/cl�nica veterin�ria"),
        JOQUEI_CLUB(8, "J�quei club"),
		LOCAL_PARA_AGLOMERACAO(9, "Local para aglomera��o"),
		PROPRIEDADE_DE_ESPERA_DE_ABATE_DE_EQUIDEOS(10, "Propriedade de espera de abate de equ�deos"),
        PROPRIEDADE_FORNECEDORA_DE_EQUIDEOS(11, "Propriedade fornecedore de equ�deos"),
        PROPRIEDADE_RURAL(12, "Propriedade rural"),
		SITIO_DE_AVES_MIGRATORIAS(13, "S�tio de aves migrat�rias"),
		SOCIEDADE_HIPICA(14,"Sociedade h�pica"),
		SOLTOS_OU_DE_PERIFERIA(15, "Soltos ou de periferia"),
		UNIDADE_DE_PESQUISA(16, "Unidade de pesquisa"),
		UNIDADE_MILITAR(17, "Unidade Militar");
		
		private long value;
		private String descricao;
		
		TipoPropriedade (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
		
		public static TipoPropriedade[] getTipoPropriedadeFormIN(){
			return new TipoPropriedade[]{ALDEIA_INDIGENA, 
					                     ASSENTAMENTO, 
					                     COMUNITARIO, 
					                     CONFINAMENTO, 
					                     HOSPITAL_VETERINARIO, 
					                     LOCAL_PARA_AGLOMERACAO, 
					                     PROPRIEDADE_RURAL, 
					                     SITIO_DE_AVES_MIGRATORIAS, 
					                     SOLTOS_OU_DE_PERIFERIA, 
					                     UNIDADE_DE_PESQUISA, 
					                     UNIDADE_MILITAR};
		}
		
		public static TipoPropriedade[] getTipoPropriedadeEquinos(){
			return new TipoPropriedade[]{ALDEIA_INDIGENA,
										 ALOJAMENTO,
					                     ASSENTAMENTO, 
					                     COMUNITARIO,  
					                     HARAS,
					                     HOSPITAL_VETERINARIO,
					                     JOQUEI_CLUB,
					                     LOCAL_PARA_AGLOMERACAO, 
					                     PROPRIEDADE_DE_ESPERA_DE_ABATE_DE_EQUIDEOS,
					                     PROPRIEDADE_FORNECEDORA_DE_EQUIDEOS,
					                     PROPRIEDADE_RURAL,
					                     SITIO_DE_AVES_MIGRATORIAS,
					                     SOCIEDADE_HIPICA, 
					                     UNIDADE_DE_PESQUISA, 
					                     UNIDADE_MILITAR};
		}
	}
	
	public enum TipoCoordenadaGeografica {
		
		SAD_69(1, "SAD 69"),
		SIRGAS_2000(2, "SIRGAS 2000"),
		WGS_84(3, "WGS 84");
		
		private long value;
		private String descricao;
		
		TipoCoordenadaGeografica(long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
		
		public static TipoCoordenadaGeografica[] getListaTipoCoordenadaGeograficaFormSN(){
			return new TipoCoordenadaGeografica[]{SIRGAS_2000, WGS_84};
		}
	}
	
	public enum TipoTelefone{
		
		CELULAR(1, "Celular"),
		COMERCIAL(2, "Comercial"),
		RESIDENCIAL(3, "Residencial");
		
		private long value;
		private String descricao;
		
		TipoTelefone (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum Status{
		
		AUTORIZADA_COORDENADORIA("Autorizada Coordenadoria"),
		AUTORIZADA_COPAS("Autorizada Copas"),
		CANCELADA("Cancelada"),
		EMITIDA("Emitida"),
		NAO_AUTORIZADA_COORDENADORIA("Nao Autorizada Coordenadoria"),
		NAO_AUTORIZADA_COPAS("Nao Autorizada Copas"),
		NOVA("Nova");
		
		private String descricao;
		
		Status (String descricao){
			this.descricao = descricao;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum SimNao{
		
		SIM("Sim"),
		NAO("Nao");
		
		private String descricao;
		
		SimNao(String descricao){
			this.descricao = descricao;
		}
		
		public String getDescricao() {
			return descricao;
		}
		
		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum TipoViagem{
		
		IDA("Ida"),
		VOLTA("Volta");
		
		private String descricao;
		
		TipoViagem(String descricao){
			this.descricao = descricao;
		}
		
		public String getDescricao() {
			return descricao;
		}
		
		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}

}