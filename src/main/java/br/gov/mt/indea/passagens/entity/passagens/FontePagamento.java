package br.gov.mt.indea.passagens.entity.passagens;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import org.hibernate.envers.Audited;

import br.gov.mt.indea.passagens.enums.Dominio.SimNao;
import br.gov.mt.indea.passagens.enums.Dominio.Status;

@Audited
@Entity
public class FontePagamento extends BaseEntity<Long> implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5961802223656128259L;

	@Id
	@SequenceGenerator(name="fonte_pagamento_seq", sequenceName="fonte_pagamento_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="fonte_pagamento_seq")
	private Long id;
	
	@Column(name="fonte")
	private Integer fonte;
	
	@Column(name="descricao")
	private String descricao;
	
	@Column(name="status")
	private Status status;
	
	@Enumerated(EnumType.STRING)
	@Column(name="fonte_propria")
	private SimNao fontePropria;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getFonte() {
		return fonte;
	}

	public void setFonte(Integer fonte) {
		this.fonte = fonte;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
	
	public SimNao getFontePropria() {
		return fontePropria;
	}

	public void setFontePropria(SimNao fontePropria) {
		this.fontePropria = fontePropria;
	}
	

}
