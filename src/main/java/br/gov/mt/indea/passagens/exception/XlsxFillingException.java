package br.gov.mt.indea.passagens.exception;

public class XlsxFillingException extends Exception {

	private static final long serialVersionUID = -5849615990643923188L;
	
	private String mensagem;
	
	public XlsxFillingException(){
		super();
	}
	
	public XlsxFillingException(String mensagem){
		super(mensagem);
		this.mensagem = mensagem;
	}
	
	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

}
