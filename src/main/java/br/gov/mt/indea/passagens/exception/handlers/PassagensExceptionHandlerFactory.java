package br.gov.mt.indea.passagens.exception.handlers;

import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerFactory;

import org.primefaces.application.exceptionhandler.PrimeExceptionHandlerFactory;

public class PassagensExceptionHandlerFactory extends PrimeExceptionHandlerFactory {

	private ExceptionHandlerFactory parent;

	public PassagensExceptionHandlerFactory(ExceptionHandlerFactory parent) {
		super(parent);
		this.parent = parent;
	}

    @Override
    public ExceptionHandler getExceptionHandler() {
        ExceptionHandler result = parent.getExceptionHandler();
        result = new PassagensExceptionHandler(result);

        return result;
    }
	
     
}
