package br.gov.mt.indea.passagens.entity.dto;

import java.io.Serializable;

import org.springframework.util.ObjectUtils;

import br.gov.mt.indea.passagens.enums.Dominio.AtivoInativo;

public class UsuarioDTO implements AbstractDTO, Serializable {

	private static final long serialVersionUID = 958534186952468759L;

	private String nome;

	private String username;
	
	private AtivoInativo status;
	
	public boolean isNull(){
		return ObjectUtils.isEmpty(this);
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public AtivoInativo getStatus() {
		return status;
	}

	public void setStatus(AtivoInativo status) {
		this.status = status;
	}

}
