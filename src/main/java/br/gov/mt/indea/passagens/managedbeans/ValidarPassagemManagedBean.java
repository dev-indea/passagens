package br.gov.mt.indea.passagens.managedbeans;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang.StringUtils;

import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.passagens.entity.passagens.Viagem;
import br.gov.mt.indea.passagens.service.ViagemService;
import br.gov.mt.indea.passagens.util.FacesMessageUtil;

@Named
@ViewScoped
@URLBeanName("validarPassagemManagedBean")
@URLMappings(mappings = {
		@URLMapping(id = "validarPassagem", pattern = "/validar", viewId = "/validarPassagem.jsf")})
public class ValidarPassagemManagedBean implements Serializable{

	private static final long serialVersionUID = -2168095796652964492L;

	private String codigo;
	
	private Viagem viagem;
	
	@Inject
	private ViagemService viagemService;
	
	@PostConstruct
	private void init(){
		
	}
	
	public void validar() {
		if (StringUtils.isEmpty(codigo)) {
			FacesMessageUtil.addWarnContextFacesMessage("C�digo n�o informado", "");
			return;
		}
		
		viagem = viagemService.findByCodigoFetchALl(codigo);
		
		if (viagem == null)
			FacesMessageUtil.addWarnContextFacesMessage("Viagem n�o encontrada", "");
			
	}
	
	public void limpar() {
		this.codigo = null;
		this.viagem = null;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public Viagem getViagem() {
		return viagem;
	}

	public void setViagem(Viagem viagem) {
		this.viagem = viagem;
	}
	
}