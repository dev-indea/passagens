package br.gov.mt.indea.passagens.service;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;

import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.passagens.entity.passagens.Contrato;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class ContratoService extends PaginableService<Contrato, Long> {
	
	private static final Logger log = LoggerFactory.getLogger(ContratoService.class);
	
	@Inject
	ServidorService servidorService;
	
	protected ContratoService() {
		super(Contrato.class);
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Contrato getInstance(){
		Contrato contrato;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select contrato ")
		   .append("  from Contrato contrato ");
		
		Query query = getSession().createQuery(sql.toString());
		contrato = (Contrato) query.uniqueResult();
		
		if (contrato != null){
		}
		
		return contrato;
	}
	
	@Override
	public void validar(Contrato Contrato) {

	}

	@Override
	public void validarPersist(Contrato Contrato) {

	}

	@Override
	public void validarMerge(Contrato Contrato) {

	}

	@Override
	public void validarDelete(Contrato Contrato) {

	}

}
