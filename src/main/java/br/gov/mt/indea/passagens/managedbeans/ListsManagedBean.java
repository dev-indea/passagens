package br.gov.mt.indea.passagens.managedbeans;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.gov.mt.indea.passagens.entity.dbindea.Unidade;
import br.gov.mt.indea.passagens.entity.passagens.UF;
import br.gov.mt.indea.passagens.enums.Dominio.AtivoInativo;
import br.gov.mt.indea.passagens.enums.Dominio.Status;
import br.gov.mt.indea.passagens.enums.Dominio.TipoCoordenadaGeografica;
import br.gov.mt.indea.passagens.enums.Dominio.TipoPropriedade;
import br.gov.mt.indea.passagens.enums.Dominio.TipoServidor;
import br.gov.mt.indea.passagens.enums.Dominio.TipoTelefone;
import br.gov.mt.indea.passagens.enums.Dominio.TipoUnidade;
import br.gov.mt.indea.passagens.service.UFService;
import br.gov.mt.indea.passagens.service.UnidadeService;

@Named
@ApplicationScoped
public class ListsManagedBean implements Serializable{

	private static final long serialVersionUID = 1096028368132582154L;

	private List<UF> listaUFs;
	
	private Map<Long, Unidade> mapUnidades;
	
	private List<Unidade> listaUnidades;
	
	@Inject
	private UFService uFService;
	
	@Inject
	private UnidadeService unidadeService;
	
	public ListsManagedBean(){
	}
	
	@PostConstruct
	public void init(){
		this.listaUFs = uFService.findAll("nome");
		this.listaUnidades = unidadeService.findAllOrderByNome();
		
		mapUnidades = new HashMap<>();
		for (Unidade unidade : listaUnidades) {
			mapUnidades.put(unidade.getId(), unidade);
		}
	}
	
	public List<UF> getListaUFs(){	
		return listaUFs;
	}
	
	public List<Unidade> getListaUnidades() {
		return listaUnidades;
	}
	
	public List<Unidade> getListaCoordenadorias(){
		return this.getListaUnidades(TipoUnidade.COORDENADORIA);
	}
	
	public List<Unidade> getListaUnidades(TipoUnidade tipoUnidade){
		List<Unidade> listaUnidades = null;
		
		if (this.getListaUnidades() != null) {
			listaUnidades = new ArrayList<Unidade>();
			for (Unidade unidade : this.getListaUnidades()) {
				if (unidade.getTipoUnidade().equals(tipoUnidade))
					listaUnidades.add(unidade);
			}
		}
		
		return listaUnidades;
	}
	
	public Unidade getUnidadeById(Long id) {
		return mapUnidades.get(id);
	}

	public TipoServidor[] getListaTipoServidor(){
		return TipoServidor.values();
	}

	public AtivoInativo[] getListaAtivoInativo(){
		return AtivoInativo.values();
	}
	
	public TipoPropriedade[] getListaTipoPropriedadeFormIN(){
		return TipoPropriedade.getTipoPropriedadeFormIN();
	}
	
	public TipoPropriedade[] getListaTipoPropriedadeEquinos(){
		return TipoPropriedade.getTipoPropriedadeEquinos();
	}
		
	public TipoCoordenadaGeografica[] getListaTipoCoordenadaGeografica(){
		return TipoCoordenadaGeografica.values();
	}
	
	public TipoTelefone[] getListaTipoTelefone(){
		return TipoTelefone.values();
	}
	
	public Status[] getListaStatus(){
		return Status.values();
	}
	
	public int[] getListaAnosFrom(int inicio){
		int[] anos;
		LocalDate data = LocalDate.now();
		int anoAtual = data.getYear();
		
		anos = new int[anoAtual - inicio + 1];
		int i = 0;
		
		
		do {
			anos[i] = anoAtual;
			i++;
			anoAtual--;
		} while (anoAtual >= inicio);
		
		return anos;
	}
	
}
