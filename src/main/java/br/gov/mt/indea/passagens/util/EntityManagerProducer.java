package br.gov.mt.indea.passagens.util;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.Default;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

import org.hibernate.Session;

import br.gov.mt.indea.passagens.annotation.IndeaWeb;

@ApplicationScoped
public class EntityManagerProducer {
	
    @PersistenceUnit(name="passagens", unitName="passagens")
    private EntityManagerFactory emfPassagens;
    
    @PersistenceUnit(name="indeaweb", unitName="indeaweb")
    private EntityManagerFactory emfIndeaWeb;

    @Produces
	@RequestScoped
	@Default
	private EntityManager getEntityManager() {
		return emfPassagens.createEntityManager();
	}
    
    @Produces
	@RequestScoped
	@IndeaWeb
	private EntityManager getEntityManagerIndeaWeb() {
		return emfIndeaWeb.createEntityManager();
	}
	
	@Produces
	@RequestScoped
	@Default
	protected Session getSession() {
		return this.getEntityManager().unwrap(Session.class);
	}
	
	@Produces
	@RequestScoped
	@IndeaWeb
	protected Session getSessionIndeaWeb() {
		return this.getEntityManagerIndeaWeb().unwrap(Session.class);
	}
	
	public void closeEntityManager(@Disposes @Any EntityManager em){
		try {
			em.close();
		} catch(Exception e){
			e.printStackTrace();
		}
	}
}
