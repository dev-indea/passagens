package br.gov.mt.indea.passagens.util;

import java.io.File;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Map;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.enterprise.event.Observes;
import javax.enterprise.event.TransactionPhase;
import javax.faces.context.FacesContext;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.SimpleEmail;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

import br.gov.mt.indea.passagens.entity.passagens.Viagem;
import br.gov.mt.indea.passagens.event.MailEvent;

@Singleton
public class Email {
	
	private static final String EMAIL_EMITENTE = "naoresponda@indea.mt.gov.br";
	
	private static final String NOME_EMITENTE = "Passagens Indea-MT";
	
	private static final String USERNAME = "app.indea.cadastroprodutor";
	
	private static final String PASSWORD = "pr0d1nd3@!";
	
	private static final String SMTP = "cuco.mt.gov.br";
	
	@Resource(mappedName = "java:jboss/mail/Gmail")
	private Session mailSession;
	
	
	public void sendSynchronousMail(@Observes(during = TransactionPhase.IN_PROGRESS) MailEvent event) {
		try {
			MimeMessage message = new MimeMessage(mailSession);
			Address[] to = new InternetAddress[] {new InternetAddress(event.getDestinatario())};

			message.setRecipients(Message.RecipientType.TO, to);
			message.setSubject(event.getAssunto());
			message.setSentDate(new Date());
			
			BodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent(event.getMensagem(), "text/html; charset=utf-8");
			
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);
			
			messageBodyPart = new MimeBodyPart();
			
			if (event.getListaAnexo() != null && !event.getListaAnexo().isEmpty()) {
				for (Map.Entry<String, FileDataSource> entry : event.getListaAnexo().entrySet()) {
					messageBodyPart.setDataHandler(new DataHandler(entry.getValue()));
					messageBodyPart.setFileName(entry.getKey() + ".pdf");
				}
				multipart.addBodyPart(messageBodyPart);
			}
			
			message.setContent(multipart);
			Transport.send(message);
		} catch (MessagingException e) {
            throw new RuntimeException(e);
        }
	}

	public static void send(String emailDestinatario, String nomeDestinatario, String assunto, String mensagem) throws EmailException {

		SimpleEmail email = new SimpleEmail();
		// Utilize o hostname do seu provedor de email
//		email.setHostName("smtp.gmail.com");
//		// Quando a porta utilizada não é a padrão (gmail = 465)
//		email.setSmtpPort(465);
//		// Adicione os destinatários
//		email.addTo(emailDestinatario, nomeDestinatario);
//		// Configure o seu email do qual enviará
//		email.setFrom(EMAIL_EMITENTE, NOME_EMITENTE);
//		// Adicione um assunto
//		email.setSubject(assunto);
//		// Adicione a mensagem do email
//		email.setMsg(mensagem);
//		email.setSSL(true);
//		// Para autenticar no servidor é necessário chamar os dois métodos abaixo
//		email.setAuthentication(USERNAME, PASSWORD);
		
		email.setHostName(SMTP);
        email.setAuthentication(USERNAME, PASSWORD);
        email.addTo(emailDestinatario, "");
        email.setFrom(EMAIL_EMITENTE, NOME_EMITENTE);
        email.setSubject(assunto);
        email.setMsg(mensagem);
        email.setStartTLSEnabled(true);
        email.getMailSession().getProperties().put("mail.smtp.ssl.trust", SMTP);
        
		email.send();
		
	}
	
	public static void sendEmailForgotPassword(String emailDestinatario, String username, String assunto, String link) throws MalformedURLException, EmailException{
		VelocityEngine ve = new VelocityEngine();
		ve.setProperty("resource.loader", "file");
		ve.setProperty("runtime.log", FacesContext.getCurrentInstance().getExternalContext().getRealPath(File.separator) + File.separator + "logs" + File.separator + "velocity.log");
		ve.setProperty("file.resource.loader.path", FacesContext.getCurrentInstance().getExternalContext().getRealPath(File.separator) + File.separator + "resources" + File.separator + "templates" + File.separator + "email");
		ve.init();
		
		Template template = ve.getTemplate("emailRedefinirPassword.vm");
		
		HtmlEmail email = new HtmlEmail();
		
		String cidImage1 = email.embed(new File(FacesContext.getCurrentInstance().getExternalContext().getRealPath(File.separator) + File.separator + "resources" + File.separator + "img" + File.separator + "logos" + File.separator + "logo_mt.png").toURI().toURL(), "MT");
		String cidImage2 = email.embed(new File(FacesContext.getCurrentInstance().getExternalContext().getRealPath(File.separator) + File.separator + "resources" + File.separator + "img" + File.separator + "logos" + File.separator + "logo_indea.png").toURI().toURL(), "Indea");
		
		VelocityContext context = new VelocityContext();
		context.put("image1", "cid:" + cidImage1);
		context.put("image2", "cid:" + cidImage2);
		context.put("username", username);
		context.put("link", link);

		StringWriter writer = new StringWriter();
		template.merge(context, writer);
		
//		email.setHostName("smtp.gmail.com");
//		email.setAuthentication(USERNAME, PASSWORD);
////		email.setStartTLSEnabled(true);
//		email.setFrom(EMAIL_EMITENTE, NOME_EMITENTE);
////		email.getMailSession().getProperties().put("mail.smtp.ssl.trust", "cuco.mt.gov.br");
//		email.setSmtpPort(465);
//		email.addTo(emailDestinatario, "");
//		email.setSubject(assunto);
//		email.setHtmlMsg(writer.toString());
//		email.setSSL(true);
		
		email.setHostName(SMTP);
        email.setAuthentication(USERNAME, PASSWORD);
        email.addTo(emailDestinatario, "");
        email.setFrom(EMAIL_EMITENTE, NOME_EMITENTE);
        email.setSubject(assunto);
        email.setHtmlMsg(writer.toString());
        email.setStartTLSEnabled(true);
        email.getMailSession().getProperties().put("mail.smtp.ssl.trust", SMTP);
        
        email.send();
	}
	
	public static void sendEmailUsuarioInativo(String emailDestinatario, String username, String assunto) throws MalformedURLException, EmailException{
		VelocityEngine ve = new VelocityEngine();
		ve.setProperty("resource.loader", "file");
		ve.setProperty("runtime.log", FacesContext.getCurrentInstance().getExternalContext().getRealPath(File.separator) + File.separator + "logs" + File.separator + "velocity.log");
		ve.setProperty("file.resource.loader.path", FacesContext.getCurrentInstance().getExternalContext().getRealPath(File.separator) + File.separator + "resources" + File.separator + "templates" + File.separator + "email");
		ve.init();
		
		Template template = ve.getTemplate("emailUsuarioInativo.vm");
		
		HtmlEmail email = new HtmlEmail();
		
		String cidImage1 = email.embed(new File(FacesContext.getCurrentInstance().getExternalContext().getRealPath(File.separator) + File.separator + "resources" + File.separator + "img" + File.separator + "logos" + File.separator + "logo_mt.png").toURI().toURL(), "MT");
		String cidImage2 = email.embed(new File(FacesContext.getCurrentInstance().getExternalContext().getRealPath(File.separator) + File.separator + "resources" + File.separator + "img" + File.separator + "logos" + File.separator + "logo_indea.png").toURI().toURL(), "Indea");
		
		VelocityContext context = new VelocityContext();
		context.put("image1", "cid:" + cidImage1);
		context.put("image2", "cid:" + cidImage2);
		context.put("username", username);

		StringWriter writer = new StringWriter();
		template.merge(context, writer);
		
//		email.setHostName("smtp.gmail.com");
//		email.setAuthentication(USERNAME, PASSWORD);
////		email.setStartTLSEnabled(true);
//		email.setFrom(EMAIL_EMITENTE, NOME_EMITENTE);
////		email.getMailSession().getProperties().put("mail.smtp.ssl.trust", "cuco.mt.gov.br");
//		email.setSmtpPort(465);
//		email.addTo(emailDestinatario, "");
//		email.setSubject(assunto);
//		email.setHtmlMsg(writer.toString());
//		email.setSSL(true);
		
		email.setHostName(SMTP);
        email.setAuthentication(USERNAME, PASSWORD);
        email.addTo(emailDestinatario, "");
        email.setFrom(EMAIL_EMITENTE, NOME_EMITENTE);
        email.setSubject(assunto);
        email.setHtmlMsg(writer.toString());
        email.setStartTLSEnabled(true);
        email.getMailSession().getProperties().put("mail.smtp.ssl.trust", SMTP);
        
        email.send();
	}
	
	/*
	 * 
	 * 
	 */
	
	public static void solicitarPassagemEmpresa(Viagem viagem, String destinatario, boolean retornarMensagem) {
		if (StringUtils.isEmpty(destinatario)) {
			
			if (retornarMensagem)
				FacesMessageUtil.addWarnContextFacesMessage("N�o foi poss�vel enviar email para o destinat�rio " + destinatario, "Email inv�lido. Verifique o cadastro do servidor/unidade no IndeaWeb");
		}else {
			try {
				Email.solicitarPassagemEmpresa(destinatario, 
						viagem.getServidor().getNome(), 
						"Passagens Indea - " + viagem.getMunicipioOrigem().getNome() + "/" + viagem.getMunicipioDestino().getNome() + " - " + " DATA: " + viagem.getDataInicio().format(java.time.format.DateTimeFormatter.ofPattern("dd/MM/yyyy")), 
						viagem);
				
				if (retornarMensagem)
					FacesMessageUtil.addInfoContextFacesMessage("Email enviado com sucesso para destinat�rio " + destinatario, "");
			} catch (MalformedURLException e) {
				if (retornarMensagem)
					FacesMessageUtil.addWarnContextFacesMessage("Erro ao enviar email para o destinat�rio " + destinatario, "Informe ao administrador do sistema");
				e.printStackTrace();
			} catch (EmailException e) {
				if (retornarMensagem)
					FacesMessageUtil.addWarnContextFacesMessage("N�o foi poss�vel enviar email para o destinat�rio " + destinatario, "Email inv�lido. Verifique o cadastro do servidor/unidade no IndeaWeb");
				
				e.printStackTrace();
			}
		}
	}
	
	private static void solicitarPassagemEmpresa(String emailDestinatario, String username, String assunto, Viagem viagem) throws MalformedURLException, EmailException{
		StringBuilder sb = new StringBuilder();
		
		String link = "http://" + FacesContext.getCurrentInstance().getExternalContext().getRequestServerName() + ":" + 
     			  FacesContext.getCurrentInstance().getExternalContext().getRequestServerPort() + 
				  FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/" + "anexarVoucher" + "/" + viagem.getAssinaturaEletronica();
		
		sb.append("<p>")
		  .append("<h3>Solicita��o de emiss�o de passagem.</h3>")
		  .append("<h4 style=\"border-bottom: 1px solid #B5B5B5\">Identifica��o do Passageiro</h4>")
		  .append("Servidor: " + viagem.getServidor().getNome())
		  .append("<br/>")
		  .append("CPF: " + viagem.getServidor().getCpf())
		  .append("<br/>");
		
		if (!StringUtils.isEmpty(viagem.getServidor().getTelefoneCelular())) {
			sb.append("Telefone: " + viagem.getServidor().getTelefoneCelular())
			  .append("<br/>");
		}
		sb.append("")
		  .append("<h4 style=\"border-bottom: 1px solid #B5B5B5\">Viagem</h4>")
		  .append("Data: " + viagem.getDataInicio().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")))
		  .append("<br/>")
		  .append("Origem: " + viagem.getMunicipioOrigem().getNome())
		  .append("<br/>")
		  .append("Destino: " + viagem.getMunicipioDestino().getNome())
		  .append("<br/>")
		  .append("Empresa de prefer�ncia: " + viagem.getEmpresa())
		  .append("<br/>")
		  .append("Observa��o do passageiro: " + viagem.getObservacao())
		  .append("")
		  .append("<h3 style=\"border-bottom: 1px solid #B5B5B5\">Autoriza��o</h3>")
		  .append("Esta passagem foi autorizada eletronicamente por:")
		  .append("<br/><br/>")
		  .append("Demandante: " + viagem.getUsuarioAprovacao().getNome())
		  .append("<br/>")
		  .append("GSG/COPAS: " + viagem.getUsuarioAutorizador().getNome())
		  .append("")
		  .append("<p style=\"border-bottom: 1px solid #B5B5B5\"></p>")
		  .append("")
		  .append("Solicitamos que fa�a a emiss�o da(s) passagem(s) e anexe os vouchers no link abaixo:")
		  .append("<br/>")
		  .append(link)
		  .append("<p>")
		  .append("Atenciosamente.")
		  .append("<br/> COPAS INDEA")
		  .append("</p>");
			
		HtmlEmail email = new HtmlEmail();
		
        email.setHostName(SMTP);
        email.setAuthentication(USERNAME, PASSWORD);
        email.addTo(emailDestinatario, "");
        email.setFrom(EMAIL_EMITENTE, NOME_EMITENTE);
        email.setSubject(assunto);
        email.setHtmlMsg(sb.toString());
        email.setStartTLSEnabled(true);
        email.getMailSession().getProperties().put("mail.smtp.ssl.trust", SMTP);

        email.send();
	}
	
}
