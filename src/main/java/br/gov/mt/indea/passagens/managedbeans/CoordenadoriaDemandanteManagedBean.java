package br.gov.mt.indea.passagens.managedbeans;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.passagens.entity.passagens.CoordenadoriaDemandante;
import br.gov.mt.indea.passagens.entity.passagens.Usuario;
import br.gov.mt.indea.passagens.service.CoordenadoriaDemandanteService;
import br.gov.mt.indea.passagens.service.UsuarioService;

@Named
@ViewScoped
@URLBeanName("coordenadoriaDemandanteManagedBean")
@URLMappings(mappings = {
		@URLMapping(id = "pesquisarCoordenadoriaDemandante", pattern = "/coordenadoriaDemandante/pesquisar", viewId = "/pages/coordenadoriaDemandante/lista.jsf"),
		@URLMapping(id = "incluirCoordenadoriaDemandante", pattern = "/coordenadoriaDemandante/incluir", viewId = "/pages/coordenadoriaDemandante/novo.jsf"),
		@URLMapping(id = "alterarCoordenadoriaDemandante", pattern = "/coordenadoriaDemandante/alterar/#{coordenadoriaDemandanteManagedBean.id}", viewId = "/pages/coordenadoriaDemandante/novo.jsf"),
		@URLMapping(id = "visualizarCoordenadoriaDemandante", pattern = "/coordenadoriaDemandante/visualizar/#{coordenadoriaDemandanteManagedBean.id}", viewId = "/pages/coordenadoriaDemandante/novo.jsf")})
public class CoordenadoriaDemandanteManagedBean implements Serializable {

	private static final long serialVersionUID = 2345719080339499530L;

	private boolean visualizando = false;
	
	private boolean editando = false;
	
	private Long id;
	
	private List<CoordenadoriaDemandante> listaCoordenadoriaDemandante;
	
	@Inject
	private CoordenadoriaDemandante coordenadoriaDemandante;
	
	@Inject
	private CoordenadoriaDemandanteService coordenadoriaDemandanteService;
	
	@Inject
	private UsuarioService usuarioService;
	
	@PostConstruct
	private void init(){
	}
	
	public void limpar(){
		this.coordenadoriaDemandante = new CoordenadoriaDemandante();
	}
	
	public void buscarCoordenadoriaDemandantes() {
		this.listaCoordenadoriaDemandante = coordenadoriaDemandanteService.findAll();
	}
	
	public List<Usuario> getListaUsuarios(){
		return this.usuarioService.findAllByNome();
	}
	
	@URLAction(mappingId = "pesquisarCoordenadoriaDemandante", onPostback = false)
	public void pesquisar(){
		limpar();
		
		this.buscarCoordenadoriaDemandantes();
	}
	
	@URLAction(mappingId = "visualizarCoordenadoriaDemandante", onPostback = false)
	public void visualizar(){
		this.visualizando = true;
		this.editando = false;
	}
	
	@URLAction(mappingId = "alterarCoordenadoriaDemandante", onPostback = false)
	public void editar(){
		this.coordenadoriaDemandante = coordenadoriaDemandanteService.findById(this.getId());
		
		this.visualizando = false;
		this.editando = true;
	}
	
	public String adicionar() {
		coordenadoriaDemandanteService.saveOrUpdate(this.coordenadoriaDemandante);
		
		return "pretty:pesquisarCoordenadoriaDemandante";
	}
	
	public void atualizarStatus(CoordenadoriaDemandante coordenadoriaDemandante) {
		coordenadoriaDemandanteService.saveOrUpdate(coordenadoriaDemandante);
		
		this.buscarCoordenadoriaDemandantes();
	}
	
	public void remover(CoordenadoriaDemandante coordenadoriaDemandante) {
		this.coordenadoriaDemandanteService.delete(coordenadoriaDemandante);
	}
	
	public boolean isVisualizando() {
		return visualizando;
	}

	public void setVisualizando(boolean visualizando) {
		this.visualizando = visualizando;
	}

	public boolean isEditando() {
		return editando;
	}

	public void setEditando(boolean editando) {
		this.editando = editando;
	}

	public CoordenadoriaDemandante getCoordenadoriaDemandante() {
		return coordenadoriaDemandante;
	}

	public void setCoordenadoriaDemandante(CoordenadoriaDemandante coordenadoriaDemandante) {
		this.coordenadoriaDemandante = coordenadoriaDemandante;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<CoordenadoriaDemandante> getListaCoordenadoriaDemandante() {
		return listaCoordenadoriaDemandante;
	}

	public void setListaCoordenadoriaDemandante(List<CoordenadoriaDemandante> listaCoordenadoriaDemandante) {
		this.listaCoordenadoriaDemandante = listaCoordenadoriaDemandante;
	}

}
