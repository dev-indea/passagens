package br.gov.mt.indea.passagens.service.seguranca;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.passagens.entity.seguranca.Grupo;
import br.gov.mt.indea.passagens.service.PaginableService;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class GrupoService extends PaginableService<Grupo, Long> {

	private static final Logger log = LoggerFactory.getLogger(GrupoService.class);

	protected GrupoService() {
		super(Grupo.class);
	}
	
	public Grupo findByIdFetchAll(Long id){
		Grupo grupo;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select grupo")
		   .append("  from Grupo grupo ")
		   .append("  left join fetch grupo.listaPermissao ")
		   .append(" where grupo.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		grupo = (Grupo) query.uniqueResult();
		
		return grupo;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Grupo> findAll(String orderBy) {
		List<Grupo> listaGrupo = null;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select grupo")
		   .append("  from Grupo grupo ")
		   .append(" order by :orderBy");
		
		Query query = getSession().createQuery(sql.toString()).setString("orderBy", orderBy);
		listaGrupo = query.list();
		
		if (listaGrupo != null){
		
			for (Grupo grupo : listaGrupo) {
				Hibernate.initialize(grupo.getListaPermissao());
			}
			
		}
		
		return listaGrupo;
	}
	
	@Override
	public void saveOrUpdate(Grupo grupo) {
		grupo = (Grupo) this.getSession().merge(grupo);
		
		super.saveOrUpdate(grupo);
		
		log.info("Salvando Grupo {}", grupo.getId());
	}
	
	@Override
	public void delete(Grupo grupo) {
		super.delete(grupo);
		
		log.info("Removendo Grupo {}", grupo.getId());
	}
	
	@Override
	public void validar(Grupo Grupo) {

	}

	@Override
	public void validarPersist(Grupo Grupo) {

	}

	@Override
	public void validarMerge(Grupo Grupo) {

	}

	@Override
	public void validarDelete(Grupo Grupo) {

	}

}
