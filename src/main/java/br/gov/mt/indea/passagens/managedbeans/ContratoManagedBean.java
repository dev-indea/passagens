package br.gov.mt.indea.passagens.managedbeans;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.passagens.entity.passagens.Contrato;
import br.gov.mt.indea.passagens.service.ContratoService;

@Named
@ViewScoped
@URLBeanName("contratoManagedBean")
@URLMappings(mappings = {@URLMapping(id = "visualizarContrato", pattern = "/contrato/visualizar", viewId = "/pages/contrato/editar.jsf"),
		@URLMapping(id = "editarContrato", pattern = "/contrato/editar", viewId = "/pages/contrato/editar.jsf")})
public class ContratoManagedBean implements Serializable {

	private static final long serialVersionUID = 5401332308697941357L;
	
	private boolean visualizando = false;
	
	private boolean editando = false;
	
	@Inject
	private Contrato contrato;
	
	@Inject
	private ContratoService contratoService;
	
	@PostConstruct
	private void init(){
		this.contrato = contratoService.getInstance();
	}
	
	public void limpar(){
		this.contrato = new Contrato();
	}
	
	@URLAction(mappingId = "visualizarContrato", onPostback = false)
	public void visualizar(){
		this.visualizando = true;
		this.editando = false;
	}
	
	@URLAction(mappingId = "editarContrato", onPostback = false)
	public void editar(){
		this.visualizando = false;
		this.editando = true;
	}
	
	public String salvar() {
		contratoService.saveOrUpdate(this.contrato);
		
		return "pretty:visualizarContrato";
	}
	
	public boolean isVisualizando() {
		return visualizando;
	}

	public void setVisualizando(boolean visualizando) {
		this.visualizando = visualizando;
	}

	public boolean isEditando() {
		return editando;
	}

	public void setEditando(boolean editando) {
		this.editando = editando;
	}

	public Contrato getContrato() {
		return contrato;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

}
