package br.gov.mt.indea.passagens.util;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

import javax.inject.Named;

@Named("dataUtil")
public class DataUtil {
	
	public static String getDate_dd_MM_yyyy(Date data){
		if (data == null)
			return null;
		
		Calendar c = Calendar.getInstance();
		c.setTime(data);
		
		return getCalendar_dd_MM_yyyy(c);
	}
	
	public static String getDate_dd_MM_yyyy_HH_MM(Date data){
		if (data == null)
			return null;
		
		Calendar c = Calendar.getInstance();
		c.setTime(data);
		
		return getData_dd_MM_yyyy_HH_MM(c);
	}
	
	public static String getTimestamp_dd_MM_yyyy(Timestamp data){
		if (data == null)
			return null;
		
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(data.getTime());
		
		return getCalendar_dd_MM_yyyy(c);
	}
	
	public static String getCalendar_dd_MM_yyyy(Calendar data){
		if (data == null)
			return null;
		
		StringBuilder sb = new StringBuilder();
		
		sb.append((data.get(Calendar.DAY_OF_MONTH) + "").length() > 1 ? data.get(Calendar.DAY_OF_MONTH) : "0" + data.get(Calendar.DAY_OF_MONTH)).append("/")
			.append(((data.get(Calendar.MONTH) + 1) + "").length() > 1 ? (data.get(Calendar.MONTH)+1) : "0" + (data.get(Calendar.MONTH)+1)).append("/")
			.append(data.get(Calendar.YEAR));
		
		return sb.toString();
	}
	
	public static String getCalendar_MM_yyyy(Calendar data){
		if (data == null)
			return null;
		
		StringBuilder sb = new StringBuilder();
		
		sb.append(((data.get(Calendar.MONTH) + 1) + "").length() > 1 ? (data.get(Calendar.MONTH)+1) : "0" + (data.get(Calendar.MONTH)+1)).append("/")
			.append(data.get(Calendar.YEAR));
		
		return sb.toString();
	}
	
	public static String getDate_yyyy_MM_dd(Date data){
		if (data == null)
			return null;
		
		Calendar c = Calendar.getInstance();
		c.setTime(data);
		
		return getData_yyyy_MM_dd(c);
	}
	
	public static String getData_yyyy_MM_dd(Calendar data){
		if (data == null)
			return null;
		
		StringBuilder sb = new StringBuilder();
		
		sb.append(data.get(Calendar.YEAR)).append("-")
			.append(((data.get(Calendar.MONTH) + 1) + "").length() > 1 ? (data.get(Calendar.MONTH)+1) : "0" + (data.get(Calendar.MONTH)+1)).append("-")
			.append((data.get(Calendar.DAY_OF_MONTH) + "").length() > 1 ? data.get(Calendar.DAY_OF_MONTH) : "0" + data.get(Calendar.DAY_OF_MONTH));
		
		return sb.toString();
	}
	
	public static String getData_dd_MM_yyyy_HH_MM(Calendar data){
		if (data == null)
			return null;
		
		StringBuilder sb = new StringBuilder();
		
		sb.append((data.get(Calendar.DAY_OF_MONTH) + "").length() > 1 ? data.get(Calendar.DAY_OF_MONTH) : "0" + data.get(Calendar.DAY_OF_MONTH)).append("/")
			.append(((data.get(Calendar.MONTH) + 1) + "").length() > 1 ? (data.get(Calendar.MONTH)+1) : "0" + (data.get(Calendar.MONTH)+1)).append("/")
			.append(data.get(Calendar.YEAR)).append(" ")
			.append((data.get(Calendar.HOUR_OF_DAY) + "").length() > 1 ? (data.get(Calendar.HOUR_OF_DAY)) : "0" + data.get(Calendar.HOUR_OF_DAY)).append(":")
			.append((data.get(Calendar.MINUTE) + "").length() > 1 ? data.get(Calendar.MINUTE) : "0" + data.get(Calendar.MINUTE));
		
		return sb.toString();
	}
	
	public static String getData_HH_MM(Calendar data){
		if (data == null)
			return null;
		
		StringBuilder sb = new StringBuilder();
		
		sb.append((data.get(Calendar.HOUR_OF_DAY) + "").length() > 1 ? (data.get(Calendar.HOUR_OF_DAY)) : "0" + data.get(Calendar.HOUR_OF_DAY)).append(":")
			.append((data.get(Calendar.MINUTE) + "").length() > 1 ? data.get(Calendar.MINUTE) : "0" + data.get(Calendar.MINUTE));
		
		return sb.toString();
	}
	
	public static String getData_HH(Calendar data){
		if (data == null)
			return null;
		
		StringBuilder sb = new StringBuilder();
		
		sb.append((data.get(Calendar.HOUR_OF_DAY) + "").length() > 1 ? (data.get(Calendar.HOUR_OF_DAY)) : "0" + data.get(Calendar.HOUR_OF_DAY));
		
		return sb.toString();
	}
	
	public static String getData_MM(Calendar data){
		if (data == null)
			return null;
		
		StringBuilder sb = new StringBuilder();
		
		sb.append((data.get(Calendar.MINUTE) + "").length() > 1 ? data.get(Calendar.MINUTE) : "0" + data.get(Calendar.MINUTE));
		
		return sb.toString();
	}

}
