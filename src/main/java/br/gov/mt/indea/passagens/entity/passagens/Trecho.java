package br.gov.mt.indea.passagens.entity.passagens;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import org.hibernate.envers.Audited;

@Audited
@Entity
public class Trecho extends BaseEntity<Long> implements Serializable{

	private static final long serialVersionUID = 6762111226968421543L;

	@Id
	@SequenceGenerator(name="trecho_seq", sequenceName="trecho_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="trecho_seq")
	private Long id;
	
	@Column(name="data_cadastro")
	private LocalDateTime dataCadastro;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="id_viagem")
	private Viagem viagem;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="id_municipio_origem")
	private Municipio municipioOrigem;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="id_municipio_destino")
	private Municipio municipioDestino;
	
	@OneToOne(fetch=FetchType.LAZY, cascade = {CascadeType.ALL})
	@JoinColumn(name="id_trecho_anexo")
	private TrechoAnexo trechoAnexo;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDateTime getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(LocalDateTime dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Viagem getViagem() {
		return viagem;
	}

	public void setViagem(Viagem viagem) {
		this.viagem = viagem;
	}

	public Municipio getMunicipioOrigem() {
		return municipioOrigem;
	}

	public void setMunicipioOrigem(Municipio municipioOrigem) {
		this.municipioOrigem = municipioOrigem;
	}

	public Municipio getMunicipioDestino() {
		return municipioDestino;
	}

	public void setMunicipioDestino(Municipio municipioDestino) {
		this.municipioDestino = municipioDestino;
	}

	public TrechoAnexo getTrechoAnexo() {
		return trechoAnexo;
	}

	public void setTrechoAnexo(TrechoAnexo trechoAnexo) {
		this.trechoAnexo = trechoAnexo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Trecho other = (Trecho) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
