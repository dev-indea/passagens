package br.gov.mt.indea.passagens.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.passagens.entity.dto.AbstractDTO;
import br.gov.mt.indea.passagens.entity.dto.UsuarioDTO;
import br.gov.mt.indea.passagens.entity.passagens.Usuario;
import br.gov.mt.indea.passagens.entity.seguranca.Grupo;
import br.gov.mt.indea.passagens.entity.seguranca.Permissao;
import br.gov.mt.indea.passagens.enums.Dominio.AtivoInativo;
import br.gov.mt.indea.passagens.util.StringUtil;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class UsuarioService extends PaginableService<Usuario, String> {
	
	private static final Logger log = LoggerFactory.getLogger(UsuarioService.class);
	
	@Inject
	ServidorService servidorService;

	protected UsuarioService() {
		super(Usuario.class);
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Usuario findById(String id){
		Usuario usuario;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select usuario ")
		   .append("  from Usuario usuario ")
		   .append(" where usuario.id = :id ");
		
		Query query = getSession().createQuery(sql.toString()).setString("id", id);
		usuario = (Usuario) query.uniqueResult();
		
		if (usuario == null){
			return null;
		}
		
		Hibernate.initialize(usuario.getListaGrupo());
		if (usuario.getListaGrupo() != null) {
			for (Grupo grupo : usuario.getListaGrupo()) {
				Hibernate.initialize(grupo.getListaPermissao());
			}
			Hibernate.initialize(usuario.getListaPermissao());
			
			if (usuario.getServidor() != null && usuario.getServidor().getIdServidor() != null)
				usuario.getServidor().setServidorIndeaWeb(servidorService.findByIdFetchAll(Long.valueOf(usuario.getServidor().getIdServidor())));
		}
		
		return usuario;
	}
	
	@SuppressWarnings("unchecked")
	public List<Usuario> findAllByNome() {
		List<Usuario> listaUsuarios;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select usuario ")
		   .append("  from Usuario usuario ")
		   .append("  left join usuario.servidor servidor")
		   .append(" order by servidor.nome");
		
		Query query = getSession().createQuery(sql.toString());
		listaUsuarios = query.list();
		
		if (listaUsuarios == null){
			return null;
		}
		
		
		return listaUsuarios;
	}
	
	public Usuario findByIdStatus(String id, AtivoInativo status){
		Usuario usuario;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select usuario ")
		   .append("  from Usuario usuario ")
		   .append(" where usuario.id = :id ")
		   .append("   and usuario.status = :status");
		
		
		
		Query query = getSession().createQuery(sql.toString()).setString("id", id).setParameter("status", status);
		usuario = (Usuario) query.uniqueResult();
		
		if (usuario == null){
			return null;
		}
		
		return usuario;
	}
	
	@Override
	public Long countAll(AbstractDTO dto) {
		StringBuilder sql = new StringBuilder();
		sql.append("select count(usuario) ")
		   .append("  from " + this.getType().getSimpleName() + " as usuario")
		   .append("  left join usuario.servidor servidor")
		   .append(" where 1 = 1");
		
		if (dto != null && !dto.isNull()){
			UsuarioDTO usuarioDTO = (UsuarioDTO) dto;
			
			if (usuarioDTO.getNome() != null && !usuarioDTO.getNome().equals(""))
				sql.append("  and lower(remove_acento(servidor.nome)) like :nome");
			if (usuarioDTO.getUsername() != null && !usuarioDTO.getUsername().equals(""))
				sql.append("  and lower(remove_acento(usuario.id)) like :username");
			if (usuarioDTO.getStatus() != null)
				sql.append(" and usuario.status = :status");
		}
		
		Query query = getSession().createQuery(sql.toString());
		
		if (dto != null && !dto.isNull()){
			UsuarioDTO usuarioDTO = (UsuarioDTO) dto;
			
			if (usuarioDTO.getNome() != null && !usuarioDTO.getNome().equals(""))
				query.setString("nome", StringUtil.removeAcentos('%' + usuarioDTO.getNome().toLowerCase()) + '%');
			if (usuarioDTO.getUsername() != null && !usuarioDTO.getUsername().equals(""))
				query.setString("username", StringUtil.removeAcentos('%' + usuarioDTO.getUsername().toLowerCase()) + '%');
			if (usuarioDTO.getStatus() != null)
				query.setParameter("status", usuarioDTO.getStatus());
		}
		
		return (Long) query.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<Usuario> findAllComPaginacao(AbstractDTO dto, int first, int pageSize, String sortField, String sortOrder) {
		StringBuilder sql = new StringBuilder();
		sql.append("select usuario ")
		   .append("  from " + this.getType().getSimpleName() + " as usuario")
		   .append("  left join fetch usuario.servidor servidor")
		   .append(" where 1 = 1");
		
		if (dto != null && !dto.isNull()){
			UsuarioDTO usuarioDTO = (UsuarioDTO) dto;
			
			if (usuarioDTO.getNome() != null && !usuarioDTO.getNome().equals(""))
				sql.append("  and lower(remove_acento(servidor.nome)) like :nome");
			if (usuarioDTO.getUsername() != null && !usuarioDTO.getUsername().equals(""))
				sql.append("  and lower(remove_acento(usuario.id)) like :username");
			if (usuarioDTO.getStatus() != null)
				sql.append(" and usuario.status = :status");
		}
		
		if (StringUtils.isNotBlank(sortField))
			sql.append(" order by " + sortField + " " + sortOrder);

		Query query = getSession().createQuery(sql.toString());
		
		if (dto != null && !dto.isNull()){
			UsuarioDTO usuarioDTO = (UsuarioDTO) dto;
			
			if (usuarioDTO.getNome() != null && !usuarioDTO.getNome().equals(""))
				query.setString("nome", StringUtil.removeAcentos('%' + usuarioDTO.getNome().toLowerCase()) + '%');
			if (usuarioDTO.getUsername() != null && !usuarioDTO.getUsername().equals(""))
				query.setString("username", StringUtil.removeAcentos('%' + usuarioDTO.getUsername().toLowerCase()) + '%');
			if (usuarioDTO.getStatus() != null)
				query.setParameter("status", usuarioDTO.getStatus());
		}

		query.setFirstResult(first);
		query.setMaxResults(pageSize);

		List<Usuario> lista = query.list();
		
		return lista;
    }
	
	@Override
	public void saveOrUpdate(Usuario usuario) {
		usuario = (Usuario) this.getSession().merge(usuario);
		
		super.saveOrUpdate(usuario);
		
		log.info("Salvando Usuario {}", usuario.getId());
	}
	
	@Override
	public void delete(Usuario usuario) {
		super.delete(usuario);
		
		log.info("Removendo Usuario {}", usuario.getId());
	}
	
	public Usuario adicionarGrupo(Usuario usuario, Grupo grupo){
		usuario.getListaGrupo().add(grupo);
		
		for (Permissao permissao : grupo.getListaPermissao()) {
			if (!usuario.getListaPermissao().contains(permissao))
				usuario.getListaPermissao().add(permissao);
		}
		
		usuario = (Usuario) this.getSession().merge(usuario);
		this.saveOrUpdate(usuario);
		
		log.info("Adicionando Usuario {} ao Grupo {}", usuario.getId(), grupo.getId());
		
		return usuario;
	}
	
	public Usuario removerGrupo(Usuario usuario, Grupo grupo){
		usuario.getListaGrupo().remove(grupo);
		
		for (Permissao permissao : grupo.getListaPermissao()) {
			if (usuario.getListaPermissao().contains(permissao))
				usuario.getListaPermissao().remove(permissao);
		}
		
		usuario = (Usuario) this.getSession().merge(usuario);
		this.saveOrUpdate(usuario);
		
		log.info("Removendo Usuario {} do Grupo {}", usuario.getId(), grupo.getId());
		
		return usuario;
	}
	
	public Usuario adicionarPermissao(Usuario usuario, Permissao permissao){
		usuario.getListaPermissao().add(permissao);
		
		usuario = (Usuario) this.getSession().merge(usuario);
		this.saveOrUpdate(usuario);
		
		log.info("Adicionando Permissao {} ao Usuario {}", permissao.getId(), usuario.getId());
		
		return usuario;
	}
	
	public Usuario removerPermissao(Usuario usuario, Permissao permissao){
		usuario.getListaPermissao().remove(permissao);
		
		usuario = (Usuario) this.getSession().merge(usuario);
		this.saveOrUpdate(usuario);
		
		log.info("Removendo Permissao {} do Usuario {}", permissao.getId(), usuario.getId());
		
		return usuario;
	}
	
	@Override
	public void validar(Usuario Usuario) {

	}

	@Override
	public void validarPersist(Usuario Usuario) {

	}

	@Override
	public void validarMerge(Usuario Usuario) {

	}

	@Override
	public void validarDelete(Usuario Usuario) {

	}

}
