package br.gov.mt.indea.passagens.service;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.List;

import javax.inject.Inject;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;

import br.gov.mt.indea.passagens.entity.passagens.BaseEntity;
import br.gov.mt.indea.passagens.exception.ApplicationException;

@SuppressWarnings({ "rawtypes" })
public abstract class BaseEntityService<T extends BaseEntity<PK>, PK extends Serializable> extends BaseService<T, PK> {

	@Inject
	private CacheUniqueFields cacheUniqueFields;
	
    protected BaseEntityService(Class<T> type) {
        super(type);
    }

    protected BaseEntityService(Class<T> type, Order... orders) {
        super(type, orders);
    }

	public void persist(T model) {
		existeObjetoRepetidoPorChave(model);
		validar((T) model);
		validarPersist((T) model);
		getSession().persist(model);
	}

	public void merge(T model) {
		existeObjetoRepetidoPorChave(model);
		validar((T) model);
		validarMerge((T) model);
		getSession().saveOrUpdate(model);
	}
	
	public void saveOrUpdate(T model) {
		existeObjetoRepetidoPorChave(model);
		validar((T) model);
		getSession().saveOrUpdate(model);
	}

	

	/**
	 * Verifica se para uma dada entity, existe algum campo único que deve ser
	 * validado antes de inclusão/alteração
	 * 
	 * @param model
	 * @see br.gov.mt.cepromat.ceprofw.common.model.UniqueValueField
	 */
	private void existeObjetoRepetidoPorChave(BaseEntity model) {

		try {

			if (!cacheUniqueFields.hasUniqueFields(model.getClass())) {
				return;
			}

			List<Field> camposUnicos = cacheUniqueFields.get(model.getClass());

			for (Field campoUnico : camposUnicos) {

				Object valor = PropertyUtils.getProperty(model, campoUnico.getName());

				boolean existe = true;

				SimpleExpression criteriaCampoUnico;
				if (valor instanceof String) {
					criteriaCampoUnico = Restrictions.eq(campoUnico.getName(), valor).ignoreCase();
				} else {
					criteriaCampoUnico = Restrictions.eq(campoUnico.getName(), valor);
				}

				// Alteração
				if (model.getId() != null) {
					existe = !findByCriteria(criteriaCampoUnico, Restrictions.not(Restrictions.idEq(model.getId()))).isEmpty();
				}
				// Inclusão
				else {
					existe = !findByCriteria(criteriaCampoUnico).isEmpty();
				}

				if (existe) {
					UniqueValueField uniqueValueField = campoUnico.getAnnotation(UniqueValueField.class);

					String fieldLabel = null;

					if (uniqueValueField != null && StringUtils.isNotBlank(uniqueValueField.fieldLabel())) {
						fieldLabel = uniqueValueField.fieldLabel();
					} else {
						fieldLabel = campoUnico.getName();
					}

					throw new ApplicationException("Registro duplicado para o campo " + fieldLabel);
				}
			}
		} catch (ApplicationException be) {
//			throw be;
			//TODO handle
		} catch (Exception e) {
			e.printStackTrace();
//			throw new ApplicationErrorException("Erro ao validar se já existe registro duplicado");
			//TODO handle
		}
	}
	

	public abstract void validar(T model);

	public abstract void validarPersist(T model);

	public abstract void validarMerge(T model);

	public abstract void validarDelete(T model);
}