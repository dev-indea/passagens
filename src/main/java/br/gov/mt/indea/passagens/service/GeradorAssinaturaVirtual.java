package br.gov.mt.indea.passagens.service;

import org.springframework.security.authentication.encoding.Md5PasswordEncoder;

import br.gov.mt.indea.passagens.entity.passagens.Viagem;

public class GeradorAssinaturaVirtual {
	
	private final static String SALT = "PASSAGENS";

	public static String gerarAssinatura(Viagem viagem) {
		StringBuilder sb = new StringBuilder();
		
		sb.append(viagem.getOs())
		  .append(viagem.getServidor().getNome())
		  .append(viagem.getMunicipioOrigem().getNome())
		  .append(viagem.getMunicipioDestino().getNome())
		  .append(viagem.getDataInicio().toString());
		
		Md5PasswordEncoder encoder = new Md5PasswordEncoder();
		
		return encoder.encodePassword(sb.toString(), SALT);
	}
}
