package br.gov.mt.indea.passagens.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;

import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.passagens.entity.passagens.CoordenadoriaDemandante;
import br.gov.mt.indea.passagens.entity.passagens.Usuario;
import br.gov.mt.indea.passagens.managedbeans.ListsManagedBean;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class CoordenadoriaDemandanteService extends PaginableService<CoordenadoriaDemandante, Long> {

	private static final Logger log = LoggerFactory.getLogger(CoordenadoriaDemandante.class);

	protected CoordenadoriaDemandanteService() {
		super(CoordenadoriaDemandante.class);
	}

	@Inject
	private ListsManagedBean listsManagedBean;

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public CoordenadoriaDemandante findById(Long id) {
		CoordenadoriaDemandante coordenadoriaDemandante;

		StringBuilder sql = new StringBuilder();
		sql.append("select coordenadoriaDemandante ")
			.append("  from CoordenadoriaDemandante as coordenadoriaDemandante ")
			.append(" where coordenadoriaDemandante.id = :id ");

		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		coordenadoriaDemandante = (CoordenadoriaDemandante) query.uniqueResult();

		if (coordenadoriaDemandante != null) {
			if (coordenadoriaDemandante.getIdUnidade() != null)
				coordenadoriaDemandante.setUnidade(listsManagedBean.getUnidadeById(Long.valueOf(coordenadoriaDemandante.getIdUnidade())));
		}

		return coordenadoriaDemandante;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<CoordenadoriaDemandante> findAll() {
		List<CoordenadoriaDemandante> listaCoordenadoriaDemandante;

		StringBuilder sql = new StringBuilder();
		sql.append("select coordenadoriaDemandante ")
			.append("  from CoordenadoriaDemandante as coordenadoriaDemandante ")
			.append(" order by coordenadoriaDemandante.nomeUnidade asc");

		Query query = getSession().createQuery(sql.toString());
		listaCoordenadoriaDemandante = query.list();

		if (listaCoordenadoriaDemandante != null) {
			for (CoordenadoriaDemandante coordenadoriaDemandante : listaCoordenadoriaDemandante) {
				if (coordenadoriaDemandante.getIdUnidade() != null)
					coordenadoriaDemandante.setUnidade(listsManagedBean.getUnidadeById(Long.valueOf(coordenadoriaDemandante.getIdUnidade())));
			}
		}

		return listaCoordenadoriaDemandante;
	}
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<CoordenadoriaDemandante> findAllAtivos() {
		List<CoordenadoriaDemandante> listaCoordenadoriaDemandante;

		StringBuilder sql = new StringBuilder();
		sql.append("select coordenadoriaDemandante ")
			.append("  from CoordenadoriaDemandante as coordenadoriaDemandante ")
			.append(" where coordenadoriaDemandante.status = 'ATIVO' ")
			.append(" order by coordenadoriaDemandante.nomeUnidade asc");

		Query query = getSession().createQuery(sql.toString());
		listaCoordenadoriaDemandante = query.list();

		if (listaCoordenadoriaDemandante != null) {
			for (CoordenadoriaDemandante coordenadoriaDemandante : listaCoordenadoriaDemandante) {
				if (coordenadoriaDemandante.getIdUnidade() != null)
					coordenadoriaDemandante.setUnidade(listsManagedBean.getUnidadeById(Long.valueOf(coordenadoriaDemandante.getIdUnidade())));
			}
		}

		return listaCoordenadoriaDemandante;
	}
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<CoordenadoriaDemandante> findAllAtivosBy(Usuario usuarioResponsavel) {
		List<CoordenadoriaDemandante> listaCoordenadoriaDemandante;

		StringBuilder sql = new StringBuilder();
		sql.append("select coordenadoriaDemandante ")
			.append("  from CoordenadoriaDemandante as coordenadoriaDemandante ")
			.append(" where coordenadoriaDemandante.status = 'ATIVO' ")
			.append("   and coordenadoriaDemandante.usuarioResponsavel = :usuarioResponsavel ")
			.append(" order by coordenadoriaDemandante.nomeUnidade asc");

		Query query = getSession().createQuery(sql.toString()).setParameter("usuarioResponsavel", usuarioResponsavel);
		listaCoordenadoriaDemandante = query.list();

		if (listaCoordenadoriaDemandante != null) {
			for (CoordenadoriaDemandante coordenadoriaDemandante : listaCoordenadoriaDemandante) {
				if (coordenadoriaDemandante.getIdUnidade() != null)
					coordenadoriaDemandante.setUnidade(listsManagedBean.getUnidadeById(Long.valueOf(coordenadoriaDemandante.getIdUnidade())));
			}
		}

		return listaCoordenadoriaDemandante;
	}

	@Override
	public void saveOrUpdate(CoordenadoriaDemandante coordenadoriaDemandante) {
		coordenadoriaDemandante = (CoordenadoriaDemandante) this.getSession().merge(coordenadoriaDemandante);

		super.saveOrUpdate(coordenadoriaDemandante);

		log.info("Salvando Coordenadoria Demandante {}", coordenadoriaDemandante.getId());
	}

	@Override
	public void delete(CoordenadoriaDemandante coordenadoriaDemandante) {
		super.delete(coordenadoriaDemandante);

		log.info("Removendo Coordenadoria Demandante {}", coordenadoriaDemandante.getId());
	}

	@Override
	public void validar(CoordenadoriaDemandante model) {
		// TODO Auto-generated method stub

	}

	@Override
	public void validarPersist(CoordenadoriaDemandante model) {
		// TODO Auto-generated method stub

	}

	@Override
	public void validarMerge(CoordenadoriaDemandante model) {
		// TODO Auto-generated method stub

	}

	@Override
	public void validarDelete(CoordenadoriaDemandante model) {
		// TODO Auto-generated method stub

	}

}
