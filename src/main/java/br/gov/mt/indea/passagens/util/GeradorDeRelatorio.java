package br.gov.mt.indea.passagens.util;

import java.util.HashMap;
import java.util.List;

import javax.faces.context.FacesContext;

import br.gov.mt.indea.passagens.entity.passagens.Contrato;
import br.gov.mt.indea.passagens.entity.passagens.Viagem;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public class GeradorDeRelatorio {
	
	
	public static byte[] gerarFormularioSolicitacaoDePassagemTerrestre(List<Viagem> listaDeViagem, Contrato contrato) throws JRException{
		JasperReport report = JasperCompileManager.compileReport(FacesContext.getCurrentInstance().getExternalContext().getRealPath("/") + "/resources/relatorios/FormularioSolicitacaoDePassagemTerrestre.jrxml");
		HashMap<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("CONTRATO", contrato);
		JasperPrint print = JasperFillManager.fillReport(report, parametros, new JRBeanCollectionDataSource(listaDeViagem));
		return JasperExportManager.exportReportToPdf(print);
	}
	
}
