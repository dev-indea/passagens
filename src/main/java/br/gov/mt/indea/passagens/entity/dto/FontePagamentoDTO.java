package br.gov.mt.indea.passagens.entity.dto;

import java.io.Serializable;

import org.springframework.util.ObjectUtils;

import br.gov.mt.indea.passagens.enums.Dominio.SimNao;
import br.gov.mt.indea.passagens.enums.Dominio.Status;

public class FontePagamentoDTO implements AbstractDTO, Serializable {

	private static final long serialVersionUID = -6902637182148275565L;
	
	private Integer fonte;
	
	private Status descricao;
	
	private SimNao isFontePropria;

	public boolean isNull(){
		return ObjectUtils.isEmpty(this);
	}

	public Integer getFonte() {
		return fonte;
	}

	public void setFonte(Integer fonte) {
		this.fonte = fonte;
	}

	public Status getDescricao() {
		return descricao;
	}

	public void setDescricao(Status descricao) {
		this.descricao = descricao;
	}

	public SimNao getIsFontePropria() {
		return isFontePropria;
	}

	public void setIsFontePropria(SimNao isFontePropria) {
		this.isFontePropria = isFontePropria;
	}
	
	
	
}
