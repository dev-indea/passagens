package br.gov.mt.indea.passagens.entity.passagens;

import java.io.Serializable;

@SuppressWarnings("serial")
public abstract class BaseEntity<PK extends Serializable> implements Serializable {
	
	public abstract PK getId();

    public abstract void setId(PK id);
	
	public String toString() {
		if (!this.getId().getClass().equals(String.class))
			return String.format("%s[id=%d]", getClass().getSimpleName(), getId());
		else
			return String.format("%s[id=%s]", getClass().getSimpleName(), getId());
	}
	
	
}
