package br.gov.mt.indea.passagens.managedbeans;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.passagens.entity.dto.FontePagamentoDTO;
import br.gov.mt.indea.passagens.entity.passagens.FontePagamento;
import br.gov.mt.indea.passagens.service.FontePagamentoService;
import br.gov.mt.indea.passagens.service.LazyObjectDataModel;
import br.gov.mt.indea.passagens.util.FacesMessageUtil;

@Named
@ViewScoped
@URLBeanName("fontePagamentoManagedBean")
@URLMappings(mappings = {
		@URLMapping(id = "pesquisarFontePagamento", pattern = "/fontepagamento/pesquisar", viewId = "/pages/fontepagamento/lista.jsf"),
		@URLMapping(id = "incluirFontePagamento", pattern = "/fontepagamento/incluir", viewId = "/pages/fontepagamento/novo.jsf"),
		@URLMapping(id = "alterarFontePagamento", pattern = "/fontepagamento/alterar/#{fontePagamentoManagedBean.id}", viewId = "/pages/fontepagamento/novo.jsf"),
		@URLMapping(id = "permissoesFontePagamento", pattern = "/fontepagamento/#{fontePagamentoManagedBean.id}/permissoes", viewId = "/pages/fontepagamento/permissoes.jsf")})
public class FontePagamentoManagedBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -304452587253375002L;

	private Long id;
	
	@Inject
	private FontePagamentoDTO fontePagamentoDTO;
	
	@Inject
	private FontePagamento fontePagamento;
	
	@Inject
	private FontePagamentoService fontePagamentoService;
	
	private LazyObjectDataModel<FontePagamento> listaFontePagamento;
	
	private boolean editando = false;
	
	@PostConstruct
	private void init(){
		
	}
	
	public void limpar(){
		this.fontePagamento = new FontePagamento();
		
	}
	
	@URLAction(mappingId = "pesquisarFontePagamento", onPostback = false)
	public void pesquisar() {
		limpar();
		
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		Long idFontePagamento = (Long) request.getSession().getAttribute("idFontePagamento");
		
		if (idFontePagamento != null){
			this.fontePagamentoDTO = new FontePagamentoDTO();
			this.buscar();
			
			request.getSession().setAttribute("idFontePagamento", null);
		}
	}
	
	@URLAction(mappingId = "alterarFontePagamento", onPostback = false)
	public void alterar() {
		this.setEditando(true);
		this.fontePagamento = fontePagamentoService.findById(this.getId());
	}
	
	public String adicionar() {
		this.fontePagamentoService.saveOrUpdate(this.fontePagamento);
		
		
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		request.getSession().setAttribute("idFontePagamento", this.fontePagamento.getId());
		
		limpar();
		
		return "pretty:pesquisarFontePagamento";
	}
	
	public void remover(FontePagamento fontePagamento) {
		this.fontePagamentoService.delete(fontePagamento);
		FacesMessageUtil.addInfoContextFacesMessage("Fonte Pagamento exclu�da com sucesso", "");
	}
	
	public void buscar() {
		this.setListaFontePagamento(new LazyObjectDataModel<FontePagamento>(this.fontePagamentoService, this.fontePagamentoDTO));
	}

	public FontePagamentoDTO getFontePagamentoDTO() {
		return fontePagamentoDTO;
	}

	public void setFontePagamentoDTO(FontePagamentoDTO fontePagamentoDTO) {
		this.fontePagamentoDTO = fontePagamentoDTO;
	}

	public LazyObjectDataModel<FontePagamento> getListaFontePagamento() {
		return listaFontePagamento;
	}

	public void setListaFontePagamento(LazyObjectDataModel<FontePagamento> listaFontePagamento) {
		this.listaFontePagamento = listaFontePagamento;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public FontePagamento getFontePagamento() {
		return fontePagamento;
	}

	public void setFontePagamento(FontePagamento fontePagamento) {
		this.fontePagamento = fontePagamento;
	}

	public FontePagamentoService getFontePagamentoService() {
		return fontePagamentoService;
	}

	public void setFontePagamentoService(FontePagamentoService fontePagamentoService) {
		this.fontePagamentoService = fontePagamentoService;
	}

	public boolean isEditando() {
		return editando;
	}

	public void setEditando(boolean editando) {
		this.editando = editando;
	}
	
}
