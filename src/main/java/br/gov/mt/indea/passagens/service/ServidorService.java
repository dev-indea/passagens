package br.gov.mt.indea.passagens.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.passagens.entity.dbindea.Servidor;
import br.gov.mt.indea.passagens.util.StringUtil;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class ServidorService extends PaginableService<Servidor, Long> {
	
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(Servidor.class);

	protected ServidorService(Class<Servidor> type) {
		super(type);
	}
	
	protected ServidorService() {
		super(Servidor.class);
	}
	
	public Servidor findByIdFetchAll(Long id) {
		StringBuilder sql = new StringBuilder();
		
		sql.append("select servidor ")
		   .append("  from Servidor as servidor")
		   .append("  left join fetch servidor.cargo")
		   .append("  left join fetch servidor.unidade")
		   .append(" WHERE servidor.id = :id ");
		
		Query query = getSession().createQuery(sql.toString());
		query.setLong("id", id);
		
		Servidor servidor = (Servidor) query.uniqueResult();
		
		return servidor;
	}
	
	@SuppressWarnings("unchecked")
	public List<Servidor> findAllOrderByNome() {
		List<Servidor> listaServidor = null;
		
		StringBuilder sql = new StringBuilder();
		sql.append("select servidor")
		   .append("  from Servidor servidor")
		   .append("  left join fetch servidor.unidade")
		   .append("  left join fetch servidor.cargo")
		   .append(" order by servidor.nome");
		
		Query query = getSession().createQuery(sql.toString());
		listaServidor = query.list();
		
		return listaServidor;
	}
	
	@SuppressWarnings("unchecked")
	public List<Servidor> findByNome(String nome) {
		StringBuilder sql = new StringBuilder();
		
		sql.append(" ")
		   .append("  from Servidor as servidor")
		   .append("  left join fetch servidor.cargo")
		   .append(" WHERE ")
		   .append(" servidor.nome LIKE :nome ");
		
		Query query = getSession().createQuery(sql.toString());
		query.setString("nome",StringUtil.removeAcentos('%'+nome.toUpperCase()+'%'));
		
		List<Servidor> listaServidor = query.list();
		
		return listaServidor;
	}
	
	@SuppressWarnings("unchecked")
	public List<Servidor> findByNomeFetchAll(String nome) {
		StringBuilder sql = new StringBuilder();
		
		sql.append(" ")
		   .append("  from Servidor as servidor")
		   .append("  left join fetch servidor.cargo")
		   .append("  left join fetch servidor.unidade")
		   .append(" WHERE ")
		   .append(" servidor.nome LIKE :nome ");
		
		Query query = getSession().createQuery(sql.toString());
		query.setString("nome",StringUtil.removeAcentos('%'+nome.toUpperCase()+'%'));
		
		List<Servidor> listaServidor = query.list();
		
		return listaServidor;
	}

	@Override
	public void validar(Servidor model) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void validarPersist(Servidor model) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void validarMerge(Servidor model) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void validarDelete(Servidor model) {
		// TODO Auto-generated method stub
		
	}

	
	
	

}
