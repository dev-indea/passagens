package br.gov.mt.indea.passagens.conversation;

import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.jboss.weld.context.http.HttpConversationContext;
import org.omnifaces.cdi.Startup;

@Startup
public class ConverstationHider {

	@Inject
	private HttpConversationContext conversationContext;

	@PostConstruct
	public void init() {
		hideConversationScope();
	}

	/**
	 * "Hide" conversation scope by replacing its default "cid" parameter name
	 * by something unpredictable.
	 */
	private void hideConversationScope() {
		conversationContext.setParameterName(UUID.randomUUID().toString());
	}

}
