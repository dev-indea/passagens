package br.gov.mt.indea.passagens.entity.passagens;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.apache.commons.lang.StringUtils;
import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name = "viagem_anexo")
public class ViagemAnexo extends BaseEntity<Long> implements Serializable{
	
	private static final long serialVersionUID = 7094777920515589056L;

	@Id
	@SequenceGenerator(name="viagem_anexo_seq", sequenceName="viagem_anexo_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="viagem_anexo_seq")
	private Long id;
	
	@Column(name = "arquivo")
	@Lob
	private byte[] arquivo;
	
	@Column(name = "tipo_arquivo")
	private String tipoArquivo;
	
	private String nome;
	
	@OneToOne(fetch=FetchType.EAGER, mappedBy = "viagemAnexo")
	private Viagem viagem;

	public ViagemAnexo(Viagem viagem) {
		this.viagem = viagem;
	}
	
	public ViagemAnexo() {
	}
	
	public String getFullFileName() {
		if (!StringUtils.isEmpty(this.nome) && !StringUtils.isEmpty(this.tipoArquivo)) {
			return this.nome + "." + this.tipoArquivo;
		}
		
		return null;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public byte[] getArquivo() {
		return arquivo;
	}

	public void setArquivo(byte[] arquivo) {
		this.arquivo = arquivo;
	}

	public String getTipoArquivo() {
		return tipoArquivo;
	}

	public void setTipoArquivo(String tipoArquivo) {
		this.tipoArquivo = tipoArquivo;
	}

	public Viagem getViagem() {
		return viagem;
	}

	public void setViagem(Viagem viagem) {
		this.viagem = viagem;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ViagemAnexo other = (ViagemAnexo) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
