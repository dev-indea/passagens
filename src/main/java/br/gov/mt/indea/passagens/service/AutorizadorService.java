package br.gov.mt.indea.passagens.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.gov.mt.indea.passagens.entity.passagens.Autorizador;
import br.gov.mt.indea.passagens.entity.passagens.Usuario;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class AutorizadorService extends PaginableService<Autorizador, Long> {

	private static final Logger log = LoggerFactory.getLogger(Autorizador.class);

	protected AutorizadorService() {
		super(Autorizador.class);
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Autorizador findById(Long id) {
		Autorizador autorizador;

		StringBuilder sql = new StringBuilder();
		sql.append("select autorizador ")
			.append("  from Autorizador as autorizador ")
			.append(" where autorizador.id = :id ");

		Query query = getSession().createQuery(sql.toString()).setLong("id", id);
		autorizador = (Autorizador) query.uniqueResult();

		if (autorizador != null) {
		}

		return autorizador;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<Autorizador> findAll() {
		List<Autorizador> listaAutorizador;

		StringBuilder sql = new StringBuilder();
		sql.append("select autorizador ")
			.append("  from Autorizador as autorizador ")
			.append("  join fetch autorizador.usuario usuario")
			.append("  join usuario.servidor servidor")
			.append(" order by servidor.nome asc");

		Query query = getSession().createQuery(sql.toString());
		listaAutorizador = query.list();

		if (listaAutorizador != null) {
		}

		return listaAutorizador;
	}
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<Autorizador> findAllAtivos() {
		List<Autorizador> listaAutorizador;

		StringBuilder sql = new StringBuilder();
		sql.append("select autorizador ")
			.append("  from Autorizador as autorizador ")
			.append("  join fetch autorizador.usuario usuario")
			.append("  join usuario.servidor servidor")
			.append(" where autorizador.status = 'ATIVO' ")
			.append(" order by servidor.nome asc");

		Query query = getSession().createQuery(sql.toString());
		listaAutorizador = query.list();

		if (listaAutorizador != null) {
		}

		return listaAutorizador;
	}
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<Autorizador> findAllAtivosBy(Usuario usuarioResponsavel) {
		List<Autorizador> listaAutorizador;

		StringBuilder sql = new StringBuilder();
		sql.append("select autorizador ")
			.append("  from Autorizador as autorizador ")
			.append("  join fetch autorizador.usuario usuario")
			.append("  join usuario.servidor servidor")
			.append(" where autorizador.status = 'ATIVO' ")
			.append("   and autorizador.usuarioResponsavel = :usuarioResponsavel ")
			.append(" order by servidor.nome asc");

		Query query = getSession().createQuery(sql.toString()).setParameter("usuarioResponsavel", usuarioResponsavel);
		listaAutorizador = query.list();

		if (listaAutorizador != null) {
		}

		return listaAutorizador;
	}

	@Override
	public void saveOrUpdate(Autorizador autorizador) {
		autorizador = (Autorizador) this.getSession().merge(autorizador);

		super.saveOrUpdate(autorizador);

		log.info("Salvando Coordenadoria Demandante {}", autorizador.getId());
	}

	@Override
	public void delete(Autorizador autorizador) {
		super.delete(autorizador);

		log.info("Removendo Coordenadoria Demandante {}", autorizador.getId());
	}

	@Override
	public void validar(Autorizador model) {
		// TODO Auto-generated method stub

	}

	@Override
	public void validarPersist(Autorizador model) {
		// TODO Auto-generated method stub

	}

	@Override
	public void validarMerge(Autorizador model) {
		// TODO Auto-generated method stub

	}

	@Override
	public void validarDelete(Autorizador model) {
		// TODO Auto-generated method stub

	}

}
