package br.gov.mt.indea.passagens.util;

import java.util.ArrayList;
import java.util.List;

public class ListUtil {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static List getList(int from, int to, List list){
		List a = new ArrayList();
		
		if (list.size() < to)
			to = list.size();
		
		for (int i = from; i < to; i++) {
			a.add(list.get(i));
		}
		
		return a;
	}

}
