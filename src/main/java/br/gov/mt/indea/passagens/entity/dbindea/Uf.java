package br.gov.mt.indea.passagens.entity.dbindea;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.gov.mt.indea.passagens.annotation.IndeaWeb;
import br.gov.mt.indea.passagens.entity.passagens.BaseEntity;

@Entity
@Table(name = "uf")
@IndeaWeb
public class Uf extends BaseEntity<Long> implements Serializable {

	private static final long serialVersionUID = -3443734207466642965L;

	@Id
    private Long id;
    
	@NotNull
    @Size(max = 20)
    @Column(nullable = false, length = 20, unique = true)
    private String nome;
    
	@NotNull
    @Size(max = 2)
    @Column(nullable = false, length = 2)
    private String sigla;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Uf other = (Uf) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
    
}