package br.gov.mt.indea.passagens.managedbeans;

import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;

import javax.faces.FacesException;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.mail.EmailException;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLBeanName;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import br.gov.mt.indea.passagens.entity.passagens.RecoverUsuario;
import br.gov.mt.indea.passagens.entity.passagens.Usuario;
import br.gov.mt.indea.passagens.enums.Dominio.AtivoInativo;
import br.gov.mt.indea.passagens.service.RecoverUsuarioService;
import br.gov.mt.indea.passagens.service.UsuarioService;
import br.gov.mt.indea.passagens.util.Email;
import br.gov.mt.indea.passagens.util.FacesMessageUtil;
import br.gov.mt.indea.passagens.util.FacesUtil;
import br.gov.mt.indea.passagens.util.TokenGenerator;

@Named("recoverUsuarioManagedBean")
@ViewScoped
@URLBeanName("recoverUsuarioManagedBean")
@URLMappings(mappings = {
		@URLMapping(id = "recuperarSenha", pattern = "/recuperarSenha", viewId = "/pages/usuario/recover/recuperarSenha.jsf"),
		@URLMapping(id = "redefinirSenha", pattern = "/redefinirSenha/#{recoverUsuarioManagedBean.token}", viewId = "/pages/usuario/recover/redefinirSenha.jsf")})
public class RecoverUsuarioManagedBean implements Serializable{
	
	private static final long serialVersionUID = 1391247080975980364L;

	private String username;
	
	private String password;
	
	private String passwordRetype;
	
	private String token;
	
	private Usuario usuario;
	
	@Inject
	private UsuarioService usuarioService;
	
	@Inject
	private RecoverUsuarioService recoverUsuarioService;

	public String forgetPassword(){
		return "/pages/usuario/recover/recuperarSenha.xhtml";
	}
	
	public String enviarEmailDeRedefinicaoDeSenha(){
		Usuario usuario = null;
		String token = null;
		
			usuario = usuarioService.findById(this.username);

		if (usuario == null){
			FacesMessageUtil.addErrorContextFacesMessage("Este usuário não existe", "");
			return "";
		}
		
		if (usuario.getStatus().equals(AtivoInativo.INATIVO)){
			try {
				Email.sendEmailUsuarioInativo(usuario.getEmail(), usuario.getNome(), "Prestação de Contas - Redefinição de senha");
			} catch (EmailException | MalformedURLException e) {
				FacesMessageUtil.addErrorContextFacesMessage("Não foi possível enviar o email para recuperação", "Entre em contato com o administrador");
				return "";
			}
		} else {
			token = TokenGenerator.csRandomAlphaNumericString(64);
			
			RecoverUsuario recoverUsuario = new RecoverUsuario();
			recoverUsuario.setUsuario(usuario);
			Md5PasswordEncoder passwordEncoder = new Md5PasswordEncoder();
			recoverUsuario.setToken(passwordEncoder.encodePassword(token, null));
			
			try {
				recoverUsuarioService.saveOrUpdate(recoverUsuario);
			} catch (Exception e) {
				FacesMessageUtil.addErrorContextFacesMessage("Não foi possível enviar o email para recuperação", "Entre em contato com o administrador, " + e.getMessage());
				return "";
			}
			
			String link = "http://" + FacesContext.getCurrentInstance().getExternalContext().getRequestServerName() + ":" + 
					       			  FacesContext.getCurrentInstance().getExternalContext().getRequestServerPort() + 
									  FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/" + "redefinirSenha" + "/" + token;
			FacesContext.getCurrentInstance().getExternalContext().getApplicationContextPath();
			FacesContext.getCurrentInstance().getExternalContext().getRealPath("/");
			FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath();
			FacesContext.getCurrentInstance().getExternalContext().getRequestPathInfo();
			FacesContext.getCurrentInstance().getExternalContext().getRequestServletPath();
			
			
			
			try {
				Email.sendEmailForgotPassword(usuario.getEmail(), usuario.getNome(), "Prestação de Contas - Redefinição de senha", link);
			} catch (EmailException | MalformedURLException e) {
				FacesMessageUtil.addErrorContextFacesMessage("Não foi possível enviar o email para recuperação", "Entre em contato com o administrador");
				return "";
			}
		}
		
		FacesMessageUtil.addInfoContextFacesMessage("Email enviado com sucesso", "Por favor siga as intruções enviadas no seu email");
		return "pretty:login";
	}
	
	@URLAction(mappingId = "redefinirSenha", onPostback = false)
	public void redefinirSenha() throws FacesException, IOException{
		Md5PasswordEncoder passwordEncoder = new Md5PasswordEncoder();
		RecoverUsuario recoverUsuario = null;
		try{
			recoverUsuario = recoverUsuarioService.findByToken(passwordEncoder.encodePassword(this.getToken(), null));
		} catch (Exception e) {
			FacesMessageUtil.addWarnContextFacesMessage("Esta solicitação não é mais válida", "");
			FacesUtil.doRedirect(FacesContext.getCurrentInstance(), "/dashboard");
			return;
		}
		
		this.usuario = recoverUsuario.getUsuario();
	}
	
	public String atualizarSenha() throws IOException{
		if (!this.password.equals(this.passwordRetype)){
			FacesMessageUtil.addWarnContextFacesMessage("As senhas digitadas devem ser iguais", "");
			return "";
		}
		
		if (this.password != null){
			
			try {
				Md5PasswordEncoder passwordEncoder = new Md5PasswordEncoder();
				
				this.password = passwordEncoder.encodePassword(this.password, null);
				usuario.setPassword(this.password);
			} catch (Exception e) {
				FacesMessageUtil.addErrorContextFacesMessage("Não foi possível criptografar a senha digitada", e.getLocalizedMessage());
				return "";
			}
		}

		this.usuarioService.saveOrUpdate(usuario);
		FacesMessageUtil.addInfoContextFacesMessage("Senha redefinida com sucesso", "");
		
		return "pretty:login";
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordRetype() {
		return passwordRetype;
	}

	public void setPasswordRetype(String passwordRetype) {
		this.passwordRetype = passwordRetype;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}
